/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_OIDIS_CHROMIUMRE_INTERFACESMAP_HPP_  // NOLINT
#define IO_OIDIS_CHROMIUMRE_INTERFACESMAP_HPP_

namespace Io {
    namespace Oidis {
        namespace ChromiumRE {
            class Application;
            class Loader;
            namespace Browser {
                class BrowserWindow;
                class BrowserWindowStdLinux;
                class BrowserWindowStdMac;
                class BrowserWindowStdWin;
                class ClientApplicationBrowser;
                class ClientHandler;
                class ClientHandlerStd;
                class ClientHandlerStdLinux;
                class DebugWindow;
                class DialogHandlerLinux;
                class MainContext;
                class MainContextImpl;
                class MainMessageLoop;
                class MainMessageLoopStd;
                class NotifyIconMac;
                class RootWindow;
                class RootWindowLinux;
                class RootWindowMac;
                class RootWindowManager;
                class RootWindowWin;
                class TaskBarMac;
                class WindowTestRunner;
                class WindowTestRunnerLinux;
                class WindowTestRunnerMac;
                class WindowTestRunnerWin;
            }
            namespace Commons {
                class ClientApplication;
                class ClientApplicationHybrid;
                class ClientApplicationOther;
                class Configuration;
                template<class T> class CookieElement;
                class CookieVisitor;
                class DialogCallback;
                class SchemeTestCommon;
                class SourceVisitor;
                class WaitableEvent;
                class WindowPosition;
            }
            namespace Connectors {
                class QueryContentHandler;
                class QueryResponse;
                class WindowHandler;
                namespace Detail {
                    class ContextMenuItemJsonProxy;
                    class ContextMenuJsonProxy;
                    class NotifyBalloonJsonProxy;
                    class NotifyIconOptionsJsonProxy;
                    class WindowHandlerFileDialog;
                }
            }
            namespace Enums {
                enum ProcessType : int;
                class TaskBarProgressState;
                class WindowStateType;
            }
            namespace Interfaces {
                namespace Delegates {
                    class IBrowserWindowDelegate;
                    class IClientApplicationDelegate;
                    class IClientDelegate;
                    class IClientRendererDelegate;
                    class IWindowDelegate;
                }
            }
            namespace Renderer {
                class ClientApplicationRenderer;
                class ClientRenderer;
            }
            namespace Structures {
                class ChromiumArgs;
            }
        }
    }
}

#endif  // IO_OIDIS_CHROMIUMRE_INTERFACESMAP_HPP_  // NOLINT
