/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_APPLICATIONDELEGATEMAC_HPP_
#define IO_OIDIS_CHROMIUMRE_APPLICATIONDELEGATEMAC_HPP_

#ifdef MAC_PLATFORM

#import <AppKit/NSApplication.h>

@interface ApplicationDelegateMac : NSObject<NSApplicationDelegate>

@end;

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_APPLICATIONDELEGATEMAC_HPP_
