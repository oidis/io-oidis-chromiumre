/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_APPLICATIONHANDLERMAC_HPP_
#define IO_OIDIS_CHROMIUMRE_APPLICATIONHANDLERMAC_HPP_

#ifdef MAC_PLATFORM

#import <AppKit/NSApplication.h>

#import "include/cef_application_mac.h"

@interface ApplicationHandlerMac : NSApplication<CefAppProtocol> {
 @private  // NOLINT
     BOOL currentlyHandlingSendEvent;
}
@end;

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_APPLICATIONHANDLERMAC_HPP_
