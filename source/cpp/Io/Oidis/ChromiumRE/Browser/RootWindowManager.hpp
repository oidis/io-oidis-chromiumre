/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOWMANAGER_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOWMANAGER_HPP_

#include "../Interfaces/Delegates/IWindowDelegate.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    /**
     * Used to create/manage RootWindow instances. The methods of this class can be
     * called from any browser process thread unless otherwise indicated.
     */
    class RootWindowManager
            : public Io::Oidis::ChromiumRE::Interfaces::Delegates::IWindowDelegate {
        typedef Io::Oidis::ChromiumRE::Connectors::QueryResponse QueryResponse;
        typedef Io::Oidis::ChromiumRE::Structures::ChromiumArgs ChromiumArgs;

     public:
        // If |terminate_when_all_windows_closed| is true quit the main message loop
        // after all windows have closed.
        explicit RootWindowManager(bool $terminateWhenAllWindowsClosed);

        // Create a new top-level native window that loads |url|.
        // If |bounds| is empty the default window size and location will be used.
        // This method can be called from anywhere to create a new top-level window.
        scoped_refptr<RootWindow> CreateRootWindow(const ChromiumArgs &$args, const string &$url);

        scoped_refptr<RootWindow> CreateRootWindow(const ChromiumArgs &$args, const string &$url, const json &$options,
                                                   const shared_ptr<QueryResponse> $response);

        scoped_refptr<RootWindow> CreateDebugWindow(const CefRefPtr<CefBrowser> &$parent);

        // Returns the RootWindow associated with the specified browser ID. Must be
        // called on the main thread.
        scoped_refptr<RootWindow> GetWindowForBrowser(int $browserId);

        scoped_refptr<RootWindow> getWindowById(const string &$id) const;

        void DestroyRootWindow(RootWindow *$rootWindow);

        // Close all existing windows. If |force| is true onunload handlers will not
        // be executed.
        void CloseAllWindows(bool $force);

     private:
        // Allow deletion via scoped_ptr only.
        friend struct base::DefaultDeleter<RootWindowManager>;

        void OnRootWindowCreated(scoped_refptr<RootWindow> $rootWindow);

        CefRefPtr<CefRequestContext> GetRequestContext(RootWindow *$rootWindow) override;

        void OnRootWindowDestroyed(RootWindow *$rootWindow) override;

        const bool terminateWhenAllWindowsClosed;

        // Existing root windows. Only accessed on the main thread.
        typedef std::set<scoped_refptr<RootWindow>> RootWindowSet;
        RootWindowSet rootWindows;

        CefRefPtr<CefRequestContext> sharedRequestContext;

        DISALLOW_COPY_AND_ASSIGN(RootWindowManager);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOWMANAGER_HPP_
