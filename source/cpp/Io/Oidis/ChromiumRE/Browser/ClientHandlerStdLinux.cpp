/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    CefRefPtr<CefDialogHandler> ClientHandlerStdLinux::GetDialogHandler() {
        return this->dialogHandler;
    }
}

#endif  // LINUX_PLATFORM
