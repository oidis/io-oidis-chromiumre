/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

#include <dwmapi.h>  // NOLINT

namespace Io::Oidis::ChromiumRE::Browser {
    using Io::Oidis::ChromiumRE::Enums::WindowStateType;
    using Io::Oidis::ChromiumRE::Connectors::QueryResponse;
    using Io::Oidis::ChromiumRE::Commons::Configuration;
    using Io::Oidis::ChromiumRE::Connectors::WindowHandler;
    using Io::Oidis::ChromiumRE::Interfaces::Delegates::IWindowDelegate;
    using Io::Oidis::ChromiumRE::Commons::WindowPosition;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    namespace UtilWin = Io::Oidis::ChromiumRE::Commons::UtilWin;

    void RootWindowWin::Show(WindowStateType $mode) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            int nCmdShow = SW_SHOWNORMAL;
            if (boost::iequals(this->id, "root")) {
                if ($mode == WindowStateType::MINIMIZED) {
                    nCmdShow = SW_SHOWMINIMIZED;
                } else if ($mode == WindowStateType::MAXIMIZED) {
                    nCmdShow = SW_SHOWMAXIMIZED;
                }
            }

            if (this->options.empty() || !this->options.value("hidden", false)) {
                ShowWindow(this->handle, nCmdShow);
                UpdateWindow(this->handle);
            }
        }
    }

    void RootWindowWin::Hide() {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            ShowWindow(this->handle, SW_HIDE);
        }
    }

    void RootWindowWin::SetBounds(int $x, int $y, size_t $width, size_t $height) {
        REQUIRE_MAIN_THREAD();

        if (this->canSetBounds()) {
            SetWindowPos(this->handle, nullptr, $x, $y, static_cast<int>($width), static_cast<int>($height), SWP_NOZORDER);
        }
    }

    void RootWindowWin::Close(bool $force) {
        REQUIRE_MAIN_THREAD();

        RootWindow::Close($force);

        if (this->handle != nullptr) {
            RECT prc{};
            GetWindowRect(this->handle, &prc);

            WINDOWPLACEMENT wp = {0};
            wp.length = sizeof(WINDOWPLACEMENT);
            GetWindowPlacement(this->handle, &wp);

            this->saveWindowPosition(prc.left, prc.top, prc.right - prc.left, prc.bottom - prc.top, wp.showCmd == SW_SHOWMAXIMIZED);

            if ($force) {
                DestroyWindow(this->handle);
            } else {
                PostMessage(this->handle, WM_CLOSE, 0, 0);
            }
        }
    }

    std::vector<HWND> winHandles;

    BOOL CALLBACK RootWindowWin::EnumWindowsProc(HWND $hwnd, LPARAM /*$lParam*/) {
        BOOL retVal = TRUE;
        if (IsWindowVisible($hwnd) != 0) {
            WINDOWPLACEMENT wp{};
            wp.length = sizeof(WINDOWPLACEMENT);
            GetWindowPlacement($hwnd, &wp);
            if (wp.showCmd == SW_SHOWNORMAL) {
                std::wstring name(255, '\0');
                GetClassName($hwnd, const_cast<wchar_t *>(name.data()), 255);
                if (boost::find_first(name, RootWindowWin::getWindowTypeName())) {
                    winHandles.push_back($hwnd);
                    retVal = FALSE;
                }
            }
        }
        return retVal;
    }

    std::vector<HWND> RootWindowWin::getWinHandles() {
        return winHandles;
    }

    bool compositionEnabled() {
        BOOL compositionEnabled = FALSE;
        bool success = DwmIsCompositionEnabled(&compositionEnabled) == S_OK;
        return (compositionEnabled != FALSE) && success;
    }

    void RootWindowWin::setShadow(HWND $hwnd, bool $enabled) {
        if (compositionEnabled()) {
            static const MARGINS shadowState[2] = {{0},
                                                   {2}};
            DwmExtendFrameIntoClientArea($hwnd, &shadowState[$enabled]);
        }
    }

    void RootWindowWin::createBrowserWindow(const string &$url) {
        this->browserWindow.reset(new BrowserWindowStdWin(this, $url));
    }

    void RootWindowWin::createRootWindow(const CefBrowserSettings &$settings) {
        REQUIRE_MAIN_THREAD();
        DCHECK(!this->handle);

        RootWindow::createRootWindow($settings);

        HINSTANCE hInstance = GetModuleHandle(nullptr);
        const std::wstring windowTitle = RootWindowWin::getWindowTypeName();
        const std::wstring windowClass = RootWindowWin::getWindowTypeName();

        const cef_color_t backgroundColor = MainContext::Get()->getBackgroundColor();
        HBRUSH backgroundBrush = CreateSolidBrush(
                RGB(CefColorGetR(backgroundColor),
                    CefColorGetG(backgroundColor),
                    CefColorGetB(backgroundColor)));

        RegisterRootClass(hInstance, windowClass, backgroundBrush);

        this->findMessageId = RegisterWindowMessage(FINDMSGSTRING);
        CHECK(this->findMessageId);

        EnumWindows(EnumWindowsProc, 0);

        auto handles = getWinHandles();
        if (getWinHandles().size() > 0) {
            WINDOWPLACEMENT wp{};
            wp.length = sizeof(WINDOWPLACEMENT);
            GetWindowPlacement(handles[0], &wp);

            HMONITOR monitor = MonitorFromRect(&wp.rcNormalPosition, MONITOR_DEFAULTTONEAREST);
            MONITORINFO info{};
            info.cbSize = sizeof(info);
            GetMonitorInfo(monitor, &info);

            RECT rect{};
            GetWindowRect(handles[0], &rect);
            const int skip = 50;
            int x = rect.left + skip;
            int y = rect.top + skip;
            int w = rect.right - rect.left;
            int h = rect.bottom - rect.top;

            if (x + w > info.rcWork.right) {
                y = y + h / 4;
                x = info.rcWork.left;

                if (y + h > info.rcWork.bottom) {
                    y = info.rcWork.top;
                }
            }
            if (y + h > info.rcWork.bottom) {
                x = x + w / 4;
                y = info.rcWork.top;

                if (x + w > info.rcWork.right) {
                    x = info.rcWork.left;
                }
            }

            this->winPosition.setX(x);
            this->winPosition.setY(y);
        }

        this->handle = CreateWindow(windowClass.c_str(), windowTitle.c_str(), WS_OVERLAPPEDWINDOW, this->winPosition.getX(),
                                    this->winPosition.getY(), this->winPosition.getWidth(), this->winPosition.getHeight(),
                                    nullptr, nullptr, hInstance, nullptr);
        CHECK(this->handle);

        SetWindowLongPtr(this->handle, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

        if (this->isHeadless()) {
            long style = GetWindowLong(this->handle, GWL_EXSTYLE);
            style |= WS_EX_TOOLWINDOW;
            style &= ~(WS_EX_APPWINDOW);
            SetWindowLong(this->handle, GWL_EXSTYLE, style);
        }

        WINDOWPLACEMENT placement{};
        GetWindowPlacement(this->handle, &placement);

        HMONITOR monitor = MonitorFromRect(&placement.rcNormalPosition, MONITOR_DEFAULTTONEAREST);
        MONITORINFO info{};
        info.cbSize = sizeof(info);
        GetMonitorInfo(monitor, &info);

        CefRect displayRect(info.rcWork.left, info.rcWork.top,
                            info.rcWork.right - info.rcWork.left,
                            info.rcWork.bottom - info.rcWork.top);
        CefRect windowRect(winPosition.getX(), winPosition.getY(), winPosition.getWidth(), winPosition.getHeight());

        if (this->isHeadless()) {
            this->modifyBounds(displayRect, windowRect);

            this->winPosition.setX(windowRect.x);
            this->winPosition.setY(windowRect.y);
            this->winPosition.setWidth(windowRect.width);
            this->winPosition.setHeight(windowRect.height);
        }

        if (boost::iequals(this->id, "root") || this->isHidden) {
            this->Hide();
        }

        UtilWin::SetUserDataPtr(this->handle, this);

        RECT rect{};
        GetClientRect(this->handle, &rect);

        SetMenu(this->handle, nullptr);

        CefRect cefRect(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);

        this->browserWindow->setIsHidden(this->isHidden);
        this->browserWindow->CreateBrowser(this->handle, cefRect, $settings, this->url, delegate->GetRequestContext(this));

        setShadow(this->handle, true);
    }

    void RootWindowWin::createRootWindowWrapper(const CefBrowserSettings &$settings) {
        if (CURRENTLY_ON_MAIN_THREAD()) {
            createRootWindow($settings);
        } else {
            MAIN_POST_CLOSURE(base::Bind(&RootWindowWin::createRootWindow, this, $settings));
        }
    }

    void RootWindowWin::RegisterRootClass(HINSTANCE $hInstance, const std::wstring &$windowClass, HBRUSH $backgroundBrush) {
        static bool classRegistered = false;
        if (classRegistered) {
            return;
        }
        classRegistered = true;

        WNDCLASSEX wcex{};

        wcex.cbSize = sizeof(WNDCLASSEX);

        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = RootWndProc;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = 0;
        wcex.hInstance = $hInstance;
        wcex.hIcon = LoadIcon($hInstance, MAKEINTRESOURCE(0));
        wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
        wcex.hbrBackground = $backgroundBrush;
        wcex.lpszMenuName = nullptr;
        wcex.lpszClassName = $windowClass.c_str();
        wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(0));

        RegisterClassEx(&wcex);
    }

    void CALLBACK RootWindowWin::DelayedSingleClick(HWND $hwnd, UINT /*unused*/, UINT_PTR $id, DWORD /*unused*/) {
        REQUIRE_MAIN_THREAD();

        DCHECK($hwnd == UtilWin::GetUserDataPtr<RootWindowWin *>($hwnd)->handle);

        KillTimer($hwnd, $id);
        WindowHandler::FireEvent("OnNotifyIconClick");
    }

    LRESULT CALLBACK RootWindowWin::RootWndProc(HWND $hWnd, UINT $message,
                                                WPARAM $wParam, LPARAM $lParam) {
        REQUIRE_MAIN_THREAD();

        RootWindowWin *self = UtilWin::GetUserDataPtr<RootWindowWin *>($hWnd);
        if (self == nullptr) {
            return DefWindowProc($hWnd, $message, $wParam, $lParam);
        }
        DCHECK($hWnd == self->handle);

        if ($message == self->findMessageId) {
            auto lpfr = reinterpret_cast<LPFINDREPLACE>($lParam);
            CHECK(lpfr == &self->findState);
            self->OnFindEvent();
            return 0;
        }

        switch ($message) {
            case WM_GETMINMAXINFO: {
                auto lpMMI = (LPMINMAXINFO)$lParam;
                WINDOWPLACEMENT wp{};
                wp.length = sizeof(WINDOWPLACEMENT);
                GetWindowPlacement(self->handle, &wp);
                if (!Io::Oidis::ChromiumRE::Commons::Configuration::getInstance().IsCanResize()) {
                    lpMMI->ptMaxTrackSize.x = lpMMI->ptMinTrackSize.x = self->winPosition.getWidthDefault();
                    lpMMI->ptMaxTrackSize.y = lpMMI->ptMinTrackSize.y = self->winPosition.getHeightDefault();
                }
                return TRUE;
            }
            case WM_NCCALCSIZE: {
                WINDOWPLACEMENT wp{};
                wp.length = sizeof(WINDOWPLACEMENT);
                GetWindowPlacement(self->handle, &wp);
                if (wp.showCmd != SW_MAXIMIZE) {
                    return 1;  // this skips WS_THICKFRAME and WS_CAPTION
                }

                if ($wParam == TRUE) {
                    auto pncc = (LPNCCALCSIZE_PARAMS)$lParam;

                    HMONITOR monitor = MonitorFromRect(&wp.rcNormalPosition, MONITOR_DEFAULTTONEAREST);
                    MONITORINFO info{};
                    info.cbSize = sizeof(info);
                    GetMonitorInfo(monitor, &info);

                    if (Io::Oidis::ChromiumRE::Commons::Configuration::getInstance().IsCanResize()) {
                        pncc->rgrc[0].left = info.rcWork.left;
                        pncc->rgrc[0].top = info.rcWork.top;
                        pncc->rgrc[0].right = info.rcWork.right;
                        pncc->rgrc[0].bottom = info.rcWork.bottom;
                        return WVR_ALIGNTOP | WVR_ALIGNLEFT | WVR_ALIGNRIGHT | WVR_ALIGNBOTTOM;
                    }

                    return 1;
                }
                break;  // run default processing task
            }
            case WM_PAINT: {
                self->OnPaint();
                return 0;
            }
            case WM_SETFOCUS: {
                self->onFocus();
                return 0;
            }
            case WM_SIZE: {
                WindowStateType type = WindowStateType::NORMAL;
                auto w = static_cast<int>($lParam & 0x0000ffff);
                auto h = static_cast<int>(($lParam >> 16) & 0x0000ffff);
                RECT wp{};
                GetWindowRect(self->handle, &wp);

                int x = wp.left;
                int y = wp.top;

                if ($wParam == SIZE_MAXIMIZED) {
                    type = WindowStateType::MAXIMIZED;
                    if (Io::Oidis::ChromiumRE::Commons::Configuration::getInstance().IsCanResize()) {
                        x = 0;
                        y = 0;
                    }
                } else if ($wParam == SIZE_MINIMIZED) {
                    type = WindowStateType::MINIMIZED;
                }

                WindowHandler::FireEvent("OnWindowChanged", json::array({json({
                                                                                      {"x",      x},
                                                                                      {"y",      y},
                                                                                      {"width",  w},
                                                                                      {"height", h},
                                                                                      {"state",  type.toString()}
                                                                              })}));

                self->OnSize($wParam == SIZE_MINIMIZED);
                break;
            }
            case WM_MOVING:
            case WM_MOVE: {
                self->onMove();
                return 0;
            }
            case WM_ERASEBKGND: {
                if (self->OnEraseBkgnd())
                    break;
                return 0;
            }
            case WM_CLOSE: {
                if (self->OnClose()) {
                    return 0;
                }
                break;
            }
            case WM_NCDESTROY: {
                UtilWin::SetUserDataPtr($hWnd, nullptr);
                self->handle = nullptr;
                self->OnDestroyed();
                return 0;
            }
            case WM_USER: {
                switch (LOWORD($lParam)) {
                    case WM_LBUTTONDBLCLK: {
                        KillTimer($hWnd, 1);
                        WindowHandler::FireEvent("OnNotifyIconDbClick");
                        return 0;
                    }
                    case WM_LBUTTONDOWN: {
                        SetTimer($hWnd, 1, GetDoubleClickTime(), DelayedSingleClick);
                        return 0;
                    }
                    case NIN_BALLOONUSERCLICK: {
                        WindowHandler::FireEvent("OnNotifyIconBalloonClick");
                        return 0;
                    }
                    case WM_CONTEXTMENU: {
                        POINT pt{};
                        GetCursorPos(&pt);
                        HMENU menu = CreatePopupMenu();
                        for (int i = 0; i < static_cast<int>(self->notifyIconContextMenuMap.size()); i++) {
                            string label = self->notifyIconContextMenuMap[i + NotifyIconBase::GetIndexModifier()].getId();
                            InsertMenu(menu,
                                       (UINT)self->notifyIconContextMenuMap[i + NotifyIconBase::GetIndexModifier()].getPosition(),
                                       MF_BYPOSITION | MF_STRING,
                                       (UINT_PTR)(NotifyIconBase::GetIndexModifier() + i),
                                       std::wstring(label.begin(), label.end()).c_str());
                        }
                        SetFocus($hWnd);
                        SendMessage($hWnd, WM_INITMENUPOPUP, (WPARAM)menu, 0);
                        auto cmd = (WORD)TrackPopupMenu(menu, TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, pt.x, pt.y, 0, $hWnd,
                                                        nullptr);

                        if (cmd != 0) {
                            SendMessage($hWnd, WM_COMMAND, cmd, 0);
                        }
                        DestroyMenu(menu);
                        return 0;
                    }
                    default:
                        break;
                }
            }
            case WM_COMMAND: {
                if (HIWORD($lParam) == 0) {
                    auto id = static_cast<int>(LOWORD($wParam));
                    if (self->notifyIconContextMenuMap.find(id) != self->notifyIconContextMenuMap.end()) {
                        WindowHandler::FireEvent("OnNotifyIconContextItemSelected",
                                                 {self->notifyIconContextMenuMap[id].getName()});
                        return 0;
                    }
                }
            }
            default:
                break;
        }

        return DefWindowProc($hWnd, $message, $wParam, $lParam);
    }

    void RootWindowWin::OnPaint() {
        PAINTSTRUCT ps{};
        BeginPaint(this->handle, &ps);
        EndPaint(this->handle, &ps);
    }

    void RootWindowWin::OnSize(bool $minimized) {
        if ($minimized) {
            if (this->browserWindow) {
                this->browserWindow->Hide();
            }
            return;
        }

        if (this->browserWindow) {
            this->browserWindow->Show();
        }

        RECT rect{};
        GetClientRect(this->handle, &rect);
        this->browserWindow->SetBounds(0, 0, (size_t)rect.right, (size_t)rect.bottom);
    }

    bool RootWindowWin::OnEraseBkgnd() {
        return (getBrowser() == nullptr);
    }

    void RootWindowWin::OnFindEvent() {
        CefRefPtr<CefBrowser> browser = getBrowser();

        if ((this->findState.Flags & FR_DIALOGTERM) == FALSE) {
            if (browser) {
                browser->GetHost()->StopFinding(true);
                this->findWhatLast.clear();
                this->findNext = false;
            }
        } else if ((this->findState.Flags & FR_FINDNEXT) && browser) {
            bool matchCase = (this->findState.Flags & FR_MATCHCASE) != 0;
            const std::wstring &findWhat = this->findBuff;
            if (matchCase != this->findMatchCaseLast || findWhat != this->findWhatLast) {
                if (!findWhat.empty()) {
                    browser->GetHost()->StopFinding(true);
                    this->findNext = false;
                }
                this->findMatchCaseLast = matchCase;
                this->findWhatLast = this->findBuff;
            }

            browser->GetHost()->Find(0, findWhat, (this->findState.Flags & FR_DOWN) != 0, matchCase, this->findNext);
            if (!this->findNext) {
                this->findNext = true;
            }
        }
    }

    bool RootWindowWin::OnClose() {
        bool retVal = false;
        if (this->browserWindow && !this->browserWindow->IsClosing()) {
            CefRefPtr<CefBrowser> browser = getBrowser();
            if (browser) {
                browser->GetHost()->CloseBrowser(false);
                retVal = true;
            }
        }

        if (boost::iequals(this->id, "root")) {
            MainContext::Get()->getRootWindowManager()->CloseAllWindows(true);
        }

        return retVal;
    }

    void RootWindowWin::OnDestroyed() {
        this->windowDestroyed = true;
        this->notifyDestroyedIfDone();
    }

    void RootWindowWin::OnBrowserCreated(CefRefPtr<CefBrowser> $browser) {
        REQUIRE_MAIN_THREAD();
        (void)$browser;

        OnSize(false);

        if (this->isHidden) {
            MainContext::Get()->getRootWindowManager()->getWindowById("root")->getBrowser()->GetHost()->SetFocus(true);
        }
    }

    void RootWindowWin::OnBrowserWindowDestroyed() {
        REQUIRE_MAIN_THREAD();

        this->browserWindow.reset();

        if (!this->windowDestroyed) {
            Close(true);
        }

        this->browserDestroyed = true;
        this->notifyDestroyedIfDone();
    }

    void RootWindowWin::OnSetTitle(const string &$title) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            SetWindowText(this->handle, CefString($title).ToWString().c_str());
        }
    }

    WindowStateType RootWindowWin::getWindowState() const {
        WINDOWPLACEMENT placement{};
        GetWindowPlacement(this->handle, &placement);

        WindowStateType retVal;
        switch (placement.showCmd) {
            case SW_MINIMIZE: {
                retVal = WindowStateType::MINIMIZED;
                break;
            }
            case SW_MAXIMIZE: {
                retVal = WindowStateType::MAXIMIZED;
                break;
            }
            default: {
                retVal = WindowStateType::NORMAL;
            }
        }
        return retVal;
    }

    void RootWindowWin::Minimize() {
        WINDOWPLACEMENT placement{};
        GetWindowPlacement(this->handle, &placement);

        if (placement.showCmd == SW_MINIMIZE) {
            this->Restore();
        } else {
            ShowWindow(this->handle, SW_MINIMIZE);
        }
    }

    void RootWindowWin::Maximize() {
        WINDOWPLACEMENT placement{};
        GetWindowPlacement(this->handle, &placement);

        if (placement.showCmd == SW_MAXIMIZE) {
            this->Restore();
        } else if (Io::Oidis::ChromiumRE::Commons::Configuration::getInstance().IsCanResize()) {
            Show(WindowStateType::MAXIMIZED);
        }
    }

    void RootWindowWin::Restore() {
        WINDOWPLACEMENT placement{};
        GetWindowPlacement(this->handle, &placement);

        if (placement.showCmd == SW_MINIMIZE) {
            ShowWindow(this->handle, SW_RESTORE);
        } else {
            Show(WindowStateType::NORMAL);
        }
    }
}

#endif  // WIN_PLATFORM
