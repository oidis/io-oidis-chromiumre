/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_CLIENTAPPLICATIONBROWSER_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_CLIENTAPPLICATIONBROWSER_HPP_

namespace Io::Oidis::ChromiumRE::Browser {
    /**
     * Client app implementation for the browser process.
     */
    class ClientApplicationBrowser
            : public Io::Oidis::ChromiumRE::Commons::ClientApplication,
              public CefBrowserProcessHandler {
     public:
        typedef std::set<CefRefPtr<Io::Oidis::ChromiumRE::Interfaces::Delegates::IClientApplicationDelegate>> DelegateSet;

        ClientApplicationBrowser();

     private:
        static void CreateDelegates(DelegateSet &$delegates);  // NOLINT

        static CefRefPtr<CefPrintHandler> CreatePrintHandler();

        void OnBeforeCommandLineProcessing(const CefString &$processType, CefRefPtr<CefCommandLine> $commandLine) override;

        CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() override {
            return this;
        }

        void OnContextInitialized() override;

        void OnBeforeChildProcessLaunch(CefRefPtr<CefCommandLine> $commandLine) override;

        void OnRenderProcessThreadCreated(CefRefPtr<CefListValue> $extraInfo) override;

        CefRefPtr<CefPrintHandler> GetPrintHandler() override {
            return printHandler;
        }

        DelegateSet delegates;

        CefRefPtr<CefPrintHandler> printHandler;

        IMPLEMENT_REFCOUNTING(ClientApplicationBrowser);
        DISALLOW_COPY_AND_ASSIGN(ClientApplicationBrowser);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_CLIENTAPPLICATIONBROWSER_HPP_
