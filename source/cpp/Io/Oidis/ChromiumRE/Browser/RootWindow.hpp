/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOW_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOW_HPP_

#include "ClientTypes.hpp"
#include "MainMessageLoop.hpp"  // DeleteOnMainThread struct is inside this header

namespace Io::Oidis::ChromiumRE::Browser {
    /**
     * Represents a top-level native window in the browser process. While references
     * to this object are thread-safe the methods must be called on the main thread
     * unless otherwise indicated.
     */
    class RootWindow
            : public base::RefCountedThreadSafe<RootWindow, DeleteOnMainThread>,
              public Io::Oidis::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate {
        typedef Io::Oidis::ChromiumRE::Connectors::QueryResponse QueryResponse;
        typedef Io::Oidis::ChromiumRE::Enums::WindowStateType WindowStateType;
        typedef Io::Oidis::ChromiumRE::Interfaces::Delegates::IWindowDelegate IWindowDelegate;
        typedef Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconBase NotifyIconBase;
        typedef Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconOptions NotifyIconOptions;
        typedef Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconContextMenuItem NotifyIconContextMenuItem;
        typedef Io::Oidis::Onion::Gui::TaskBar::TaskBarBase TaskBarBase;
        typedef Io::Oidis::ChromiumRE::Structures::ChromiumArgs ChromiumArgs;

     public:
        using NotificationIconContextMenu = std::map<int, NotifyIconContextMenuItem>;

        /**
         * Create a new RootWindow object. This method may be called on any thread.
         * Use RootWindowManager::CreateRootWindow() or CreateRootWindowAsPopup() instead of calling this method directly.
         * @param $useViews Specify true tu use views framework false otherwise.
         * @return
         */
        static scoped_refptr<RootWindow> Create(bool $useViews);

        /**
         * Returns the RootWindow associated with the specified |browser_id|. Must be called on the main thread.
         * @param $browserId Specify browser ID.
         * @return Returns RootWindow reference pointer for window attached with browser ID.
         */
        static scoped_refptr<RootWindow> GetForBrowser(int $browserId);

        RootWindow();

        virtual ~RootWindow() = default;

        /**
         * Initialize as a normal window. This will create and show a native window hosting a single browser instance.
         * This method may be called on any thread. Use RootWindowManager::CreateRootWindow() instead of calling this method directly.
         * @param $delegate Specify non-NULL delegate.
         * @param $bounds Specify new window bounds.
         * @param $settings Set browser settings.
         * @param $url Specify startup URL.
         */
        void Init(IWindowDelegate *$delegate, const ChromiumArgs &$args, const CefBrowserSettings &$settings, const string &$url);

        /**
         * Initialize as a normal window. This will create and show a native window hosting a single browser instance.
         * This method may be called on any thread. Use RootWindowManager::CreateRootWindow() instead of calling this method directly.
         * @param $delegate Specify non-NULL delegate.
         * @param $bounds Specify new window bounds.
         * @param $settings Set browser settings.
         * @param $url Specify startup URL.
         * @param $options Specify window options, mainly used for child window with javascript invoke.
         * @param $response Specify query response.
         */
        void Init(IWindowDelegate *$delegate, const ChromiumArgs &$args, const CefBrowserSettings &$settings, const string &$url,
                  const json &$options, const shared_ptr<QueryResponse> $response);

        /**
         * Show the window.
         * @param $mode Specify show mode.
         */
        virtual void Show(WindowStateType $mode) = 0;

        /**
         * Hide the window.
         */
        virtual void Hide() = 0;

        /**
         * Set the window bounds in screen coordinates.
         * @param $x Specify X location.
         * @param $y Specify Y location.
         * @param $width Specify window width.
         * @param $height Specify window height.
         */
        virtual void SetBounds(int $x, int $y, size_t $width, size_t $height) = 0;

        /**
         * Close the window.
         * @param $force Set true to force close window (OnUnload handler will not be executed).
         */
        virtual void Close(bool $force) = 0;

        /**
         * @return Returns the browser that this window contains, if any.
         */
        CefRefPtr<CefBrowser> getBrowser() const;

        /**
         * @return Returns the native handle for this window, if any.
         */
        ClientWindowHandle getWindowHandle() const;

        /**
         * @return Returns window ID.
         */
        const string &getId() const {
            return this->id;
        }

        virtual WindowStateType getWindowState() const = 0;

        bool isDestroyed() const;

        /**
         * Execute script in this window.
         * @param $options Specify script and its options.
         * @param $response Specify response.
         */
        virtual void ExecuteScript(const json &$options, const shared_ptr<QueryResponse> $response);

        /**
         * Sends response from executed script.
         * @param $response A response message to be send.
         */
        virtual void SendScriptResponse(const string &$response) const;

        /**
         * @return Returns pointer to attached notify icon.
         */
        const shared_ptr<NotifyIconBase> &getNotifyIcon() const;

        /**
         * Creates notify icon if not exist for that window.
         * @param $options Specify notify icon configuration.
         * @param $response Specify query response.
         * @return Returns true if succeed, false otherwise.
         */
        virtual bool CreateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options,
                                      const shared_ptr<QueryResponse> $response);

        /**
         * Modify existing notify icon or create new one.
         * @param $options Specify notify icon configuration.
         * @param $response Specify query response.
         * @return Returns true if succeed, false otherwise.
         */
        virtual bool ModifyNotifyIcon(std::shared_ptr<NotifyIconOptions> $options,
                                      const shared_ptr<QueryResponse> $response);

        /**
         * Destroy existing notify icon.
         * @param $response Specify query response.
         * @return Returns true if succeed, false otherwise.
         */
        virtual bool DestroyNotifyIcon(const shared_ptr<QueryResponse> $response);

        const shared_ptr<TaskBarBase> &getTaskBar() const;

        void setTaskBar(const shared_ptr<TaskBarBase> &$taskBar);

        virtual void Minimize() = 0;

        virtual void Maximize() = 0;

        virtual void Restore() = 0;

        void OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) override;

        bool isHeadless() const {
            return this->headless;
        }

     protected:
        friend struct DeleteOnMainThread;

        friend class base::RefCountedThreadSafe<RootWindow, DeleteOnMainThread>;

        ClientWindowHandle handle = nullptr;
        string id;
        IWindowDelegate *delegate = nullptr;
        scoped_ptr<Io::Oidis::ChromiumRE::Browser::BrowserWindow> browserWindow;
        CefString url;
        bool windowDestroyed = false;
        bool browserDestroyed = false;
        bool isHidden = false;
        bool headless = false;
        Io::Oidis::ChromiumRE::Commons::WindowPosition winPosition;

        shared_ptr<NotifyIconBase> notifyIcon = nullptr;
        shared_ptr<TaskBarBase> taskBar = nullptr;

        shared_ptr<QueryResponse> queryResponse = nullptr;
        json scriptOptions = json();
        json options;
        shared_ptr<QueryResponse> scriptResponse = nullptr;
        bool scriptIdle = false;

        NotificationIconContextMenu notifyIconContextMenuMap;

        void updateNotifyIconPopup(const std::shared_ptr<NotifyIconOptions> &$options);

        void modifyBounds(const CefRect &$display, CefRect &$window) const;

        bool canSetBounds() const;

        void onFocus() const;

        void onMove() const;

        void saveWindowPosition(int $x, int $y, int $width, int $height, bool $maximized);

        void loadWindowPosition();

        void notifyDestroyedIfDone();

        bool closeBrowser();

        virtual void createBrowserWindow(const string &$url) = 0;

        virtual void createRootWindow(const CefBrowserSettings &$settings) = 0;

        virtual void createRootWindowWrapper(const CefBrowserSettings &$settings) = 0;
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOW_HPP_
