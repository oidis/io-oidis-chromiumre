/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_CLIENTHANDLERSTDLINUX_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_CLIENTHANDLERSTDLINUX_HPP_

#ifdef LINUX_PLATFORM

#include "DialogHandlerLinux.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    class ClientHandlerStdLinux
            : public Io::Oidis::ChromiumRE::Browser::ClientHandlerStd {
     public:
        using ClientHandlerStd::ClientHandlerStd;

     private:
        CefRefPtr<CefDialogHandler> GetDialogHandler() override;

        CefRefPtr<DialogHandlerLinux> dialogHandler = new DialogHandlerLinux();

        IMPLEMENT_REFCOUNTING(ClientHandlerStdLinux);
        DISALLOW_COPY_AND_ASSIGN(ClientHandlerStdLinux);
    };
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_CLIENTHANDLERSTDLINUX_HPP_
