/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDWIN_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDWIN_HPP_

#ifdef WIN_PLATFORM

namespace Io::Oidis::ChromiumRE::Browser {
    class BrowserWindowStdWin
            : public Io::Oidis::ChromiumRE::Browser::BrowserWindow {
     public:
        BrowserWindowStdWin(Io::Oidis::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate *$delegate,
                            const string &$startupUrl);

        void CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect,
                           const CefBrowserSettings &$settings, const CefString &$url,
                           CefRefPtr<CefRequestContext> $requestContext) override;

        void Show() override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        ClientWindowHandle GetWindowHandle() const override;

     private:
        DISALLOW_COPY_AND_ASSIGN(BrowserWindowStdWin);
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDWIN_HPP_
