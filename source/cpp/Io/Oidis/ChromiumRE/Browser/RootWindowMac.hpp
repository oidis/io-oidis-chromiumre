/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOWMAC_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOWMAC_HPP_

#ifdef MAC_PLATFORM

#include "RootWindow.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    class RootWindowMac
            : public Io::Oidis::ChromiumRE::Browser::RootWindow {
        typedef Io::Oidis::ChromiumRE::Enums::WindowStateType WindowStateType;

     public:
        using RootWindow::RootWindow;

        ~RootWindowMac();

        void Show(WindowStateType $mode) override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        void Close(bool $force) override;

        WindowStateType getWindowState() const override;

        void Minimize() override;

        void Maximize() override;

        void Restore() override;

     private:
        void createBrowserWindow(const string &$url) override;

        void createRootWindow(const CefBrowserSettings &$settings) override;

        void createRootWindowWrapper(const CefBrowserSettings &$settings) override;

        void OnSize();

        void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBrowserWindowDestroyed() override;

        void OnSetTitle(const string &$title) override;

        DISALLOW_COPY_AND_ASSIGN(RootWindowMac);
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_ROOTWINDOWMAC_HPP_
