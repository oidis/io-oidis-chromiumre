/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDMAC_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDMAC_HPP_

#ifdef MAC_PLATFORM

namespace Io::Oidis::ChromiumRE::Browser {
    class BrowserWindowStdMac
            : public Io::Oidis::ChromiumRE::Browser::BrowserWindow {
     public:
        BrowserWindowStdMac(Io::Oidis::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate *$delegate,
                            const string &$startupUrl);

        void CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect, const CefBrowserSettings &$settings,
                           const CefString &$url, CefRefPtr<CefRequestContext> $requestContext) override;

        void Show() override;

        void Hide() override;

        void SetBounds(int $x, int $y, size_t $width, size_t $height) override;

        ClientWindowHandle GetWindowHandle() const override;

     private:
        DISALLOW_COPY_AND_ASSIGN(BrowserWindowStdMac);
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOWSTDMAC_HPP_
