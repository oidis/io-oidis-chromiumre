/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#import <Cocoa/Cocoa.h>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    using Io::Oidis::ChromiumRE::Enums::WindowCornerType;

    namespace UtilMac = Io::Oidis::ChromiumRE::Commons::UtilMac;

    CefRect WindowTestRunner::getRectangleForMove(ClientWindowHandle $handle, int $x, int $y) {
        return { $x,
                 static_cast<int>(UtilMac::GetVisibleFrame($handle).size.height - $handle.frame.size.height - $y),
                 static_cast<int>($handle.frame.size.width),
                 static_cast<int>($handle.frame.size.height) };
    }

    CefRect WindowTestRunner::getRectangleForResize(ClientWindowHandle $handle, int $dx, int $dy, WindowCornerType $fixedCorner) {
        NSRect updatedFrame = $handle.frame;

        switch ($fixedCorner) {
            case WindowCornerType::TOP_LEFT:
                updatedFrame.origin.y -= $dy;
                updatedFrame.size.height += $dy;
                updatedFrame.size.width += $dx;

                break;
            case WindowCornerType::TOP_RIGHT:
                updatedFrame.origin.x += $dx;
                updatedFrame.origin.y += $dy;
                updatedFrame.size.width -= $dx;
                updatedFrame.size.height -= $dy;

                break;
            case WindowCornerType::BOTTOM_LEFT:
                updatedFrame.size.width += $dx;
                updatedFrame.size.height += $dy;

                break;
            case WindowCornerType::BOTTOM_RIGHT:
                updatedFrame.origin.x += $dx;
                updatedFrame.size.width -= $dx;
                updatedFrame.size.height -= $dy;

                break;
        }

        return { static_cast<int>(updatedFrame.origin.x),
                 static_cast<int>(updatedFrame.origin.y),
                 static_cast<int>(updatedFrame.size.width),
                 static_cast<int>(updatedFrame.size.height) };
    }

    void WindowTestRunner::performOnCloseActions(RootWindow *$rootWindow) {
        if ($rootWindow->isDestroyed()) {
            MainContext::Get()->getRootWindowManager()->DestroyRootWindow($rootWindow);
        }
    }
}

#endif  // MAC_PLATFORM
