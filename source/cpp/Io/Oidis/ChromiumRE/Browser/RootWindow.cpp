/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include "../sourceFilesMap.hpp"

#undef RootWindow

namespace Io::Oidis::ChromiumRE::Browser {
    using Io::Oidis::XCppCommons::Utils::LogIt;
    using Io::Oidis::ChromiumRE::Connectors::Detail::NotifyIconOptionsJsonProxy;
    using Io::Oidis::ChromiumRE::Commons::SourceVisitor;
    using Io::Oidis::ChromiumRE::Commons::Configuration;
    using Io::Oidis::ChromiumRE::Commons::WaitableEvent;
    namespace Gui = Io::Oidis::Onion::Gui;

    RootWindow::RootWindow() {
        if (this->id.empty() && MainContext::Get()->getRootWindowManager()->getWindowById("root") == nullptr) {
            this->id = "root";
        }
    }

    void RootWindow::Init(IWindowDelegate *$delegate, const ChromiumArgs &$args, const CefBrowserSettings &$settings, const string &$url) {
        DCHECK($delegate);

        this->delegate = $delegate;

        this->headless = $args.getHeadless();

        this->createBrowserWindow($url);

        this->url = CefString($url);
        this->options = json();

        this->createRootWindowWrapper($settings);
    }

    void RootWindow::Init(IWindowDelegate *$delegate, const ChromiumArgs &$args, const CefBrowserSettings &$settings,
                          const string &$url, const json &$options, const shared_ptr<QueryResponse> $response) {
        if (!$options.empty() && $options.value("hidden", false)) {
            this->isHidden = true;
        }
        boost::uuids::random_generator gen;
        this->id = boost::uuids::to_string(gen());

        this->Init($delegate, $args, $settings, $url);

        this->options = $options;
        this->queryResponse = $response;
    }

    void RootWindow::Close(bool $force) {
        (void)$force;
        this->DestroyNotifyIcon(nullptr);

        if (this->taskBar != nullptr) {
            this->taskBar->Clear();
        }
    }

    CefRefPtr<CefBrowser> RootWindow::getBrowser() const {
        REQUIRE_MAIN_THREAD();

        if (this->browserWindow != nullptr) {
            return this->browserWindow->GetBrowser();
        }

        return nullptr;
    }

    ClientWindowHandle RootWindow::getWindowHandle() const {
        return this->handle;
    }

    bool RootWindow::isDestroyed() const {
        return this->windowDestroyed;
    }

    void RootWindow::ExecuteScript(const json &$options, const shared_ptr<QueryResponse> $response) {
        const string script = $options.value("script", "");
        if (!script.empty()) {
            this->scriptResponse = $response;
            if (!this->getBrowser()->IsLoading()) {
                $response->Send(json{{"type", "onstart"}});

                this->getBrowser()->GetMainFrame()->ExecuteJavaScript(script, this->getBrowser()->GetMainFrame()->GetURL(), 0);

                $response->Send(json{{"type",     "oncomplete"},
                                     {"windowId", this->getId()}});

                this->scriptIdle = false;
            } else {
                this->scriptOptions = $options;
                this->scriptIdle = true;
            }
        } else {
            $response->Send(false);
        }
    }

    void RootWindow::SendScriptResponse(const string &$response) const {
        if (this->scriptResponse) {
            json response = {{"type",     "onchange"},
                             {"windowId", this->getId()},
                             {"data",     $response}};
            LogIt::Debug("Sending script response from window {0}\n{1}", this->getId(), response.dump());
            this->scriptResponse->Send(response);
        }
    }

    scoped_refptr<RootWindow> RootWindow::GetForBrowser(int $browserId) {
        return MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browserId);
    }

    const shared_ptr<RootWindow::NotifyIconBase> &RootWindow::getNotifyIcon() const {
        return this->notifyIcon;
    }

    bool RootWindow::CreateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options,
                                      const shared_ptr<QueryResponse> $response) {
        this->notifyIcon.reset(Gui::NotifyIcon::NotifyIconFactory::Create());
        this->updateNotifyIconPopup($options);
        return this->notifyIcon->Create($options);
    }

    bool RootWindow::ModifyNotifyIcon(std::shared_ptr<NotifyIconOptions> $options,
                                      const shared_ptr<QueryResponse> $response) {
        if (!this->notifyIcon) {
            this->notifyIcon.reset(Gui::NotifyIcon::NotifyIconFactory::Create());
        }
        this->updateNotifyIconPopup($options);
        return notifyIcon->Modify($options);
    }

    bool RootWindow::DestroyNotifyIcon(const shared_ptr<QueryResponse> $response) {
        bool status = false;
        if (this->notifyIcon != nullptr) {
            status = this->notifyIcon->Destroy();
            this->notifyIcon = nullptr;
        }
        return status;
    }

    const shared_ptr<RootWindow::TaskBarBase> &RootWindow::getTaskBar() const {
        return this->taskBar;
    }

    void RootWindow::setTaskBar(const shared_ptr<TaskBarBase> &$taskBar) {
        this->taskBar = $taskBar;
    }

    void RootWindow::OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) {
        (void)$canGoBack;
        (void)$canGoForward;

        REQUIRE_MAIN_THREAD();

        if (!$isLoading) {
            if (this->queryResponse) {
                CefRefPtr<SourceVisitor> visitor = new SourceVisitor();
                visitor->setBrowser(this->getBrowser());
                visitor->setResponse(this->queryResponse);
                visitor->setRootId(this->getId());
                this->getBrowser()->GetMainFrame()->GetSource(visitor);
            }
            if (!this->scriptOptions.empty()) {
                if (this->scriptIdle) {
                    this->scriptIdle = false;
                    this->scriptResponse->Send(json{{"type", "onstart"}});

                    this->getBrowser()->GetMainFrame()->ExecuteJavaScript(this->scriptOptions["script"].get<string>(),
                                                                          this->getBrowser()->GetMainFrame()->GetURL(), 0);

                    this->scriptResponse->Send(json{{"type",     "oncomplete"},
                                                    {"windowId", this->getId()}});
                }
                this->scriptOptions = json();
            }

            if (!this->isHidden) {
                if (this->winPosition.getIsMaximized()) {
                    Show(WindowStateType::MAXIMIZED);
                } else {
                    Show(WindowStateType::NORMAL);
                }
            }
        }
    }

    void RootWindow::updateNotifyIconPopup(const std::shared_ptr<NotifyIconOptions> &$options) {
        this->notifyIconContextMenuMap.clear();

        for (int i = 0; i < static_cast<int>($options->getContextMenu().size()); i++) {
            this->notifyIconContextMenuMap.emplace(NotifyIconBase::GetIndexModifier() + i, $options->getContextMenu()[i]);
        }
    }

    void RootWindow::modifyBounds(const CefRect &$display, CefRect &$window) const {
        if ($window.x < $display.x) {
            $window.x = $display.x + ($display.width - $window.width) / 2;
            $window.y = $display.y + ($display.height - $window.height) / 2;
        }
        if ($window.y < $display.y) {
            $window.x = $display.x + ($display.width - $window.width) / 2;
            $window.y = $display.y + ($display.height - $window.height) / 2;
        }
        if ($window.width < 100) {
            $window.width = 100;
        } else if ($window.width >= $display.width) {
            $window.width = $display.width;
        }
        if ($window.height < 100) {
            $window.height = 100;
        } else if ($window.height >= $display.height) {
            $window.height = $display.height;
        }

        if ($window.x < 0) {
            $window.x = 0;
        }
        if ($window.y < 0) {
            $window.y = 0;
        }

        if ($window.x + $window.width >= $display.x + $display.width) {
            $window.x = $display.x + $display.width - $window.width;
        }
        if ($window.y + $window.height >= $display.y + $display.height) {
            $window.y = $display.y + $display.height - $window.height;
        }
    }

    bool RootWindow::canSetBounds() const {
        return this->handle != nullptr && !this->isHidden && Configuration::getInstance().IsCanResize();
    }

    void RootWindow::onFocus() const {
        if (this->browserWindow != nullptr) {
            this->browserWindow->SetFocus(true);
        }
    }

    void RootWindow::onMove() const {
        if (!this->isHeadless()) {
            CefRefPtr<CefBrowser> browser = this->getBrowser();
            if (browser != nullptr) {
                browser->GetHost()->NotifyMoveOrResizeStarted();
            }
        }
    }

    void RootWindow::saveWindowPosition(int $x, int $y, int $width, int $height, bool $maximized) {
        this->winPosition.setIsMaximized($maximized);

        if (!this->winPosition.getIsMaximized()) {
            this->winPosition.setX($x);
            this->winPosition.setY($y);
            this->winPosition.setWidth($width);
            this->winPosition.setHeight($height);
        }

        if (boost::iequals(this->id, "root")) {
            this->winPosition.Save(CefCookieManager::GetGlobalManager(nullptr));
        }
    }

    void RootWindow::loadWindowPosition() {
        CefRefPtr<CefCookieManager> manager = CefCookieManager::GetGlobalManager(nullptr);

        this->winPosition.Load(manager, this);
    }

    void RootWindow::notifyDestroyedIfDone() {
        if (this->windowDestroyed && this->browserDestroyed) {
            this->delegate->OnRootWindowDestroyed(this);
        }
    }

    bool RootWindow::closeBrowser() {
        bool browserAskedToClose = false;

        if (this->browserWindow != nullptr && !this->browserWindow->IsClosing() && !this->browserDestroyed) {
            CefRefPtr<CefBrowser> browser = this->getBrowser();

            if (browser != nullptr) {
                browser->GetHost()->CloseBrowser(false);
                browserAskedToClose = true;
            }
        }

        return browserAskedToClose;
    }

    void RootWindow::createRootWindow(const CefBrowserSettings &$settings) {
        this->loadWindowPosition();
    }
}
