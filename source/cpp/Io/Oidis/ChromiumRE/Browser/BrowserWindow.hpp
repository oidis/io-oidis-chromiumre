/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOW_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOW_HPP_

#include "ClientTypes.hpp"
#include "ClientHandler.hpp"
#include "../Interfaces/Delegates/IBrowserWindowDelegate.hpp"
#include "../Interfaces/Delegates/IClientDelegate.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    /**
     * Represents a native child window hosting a single browser instance.
     * The methods of this class must be called on the main thread unless otherwise indicated.
     */
    class BrowserWindow
            : public Io::Oidis::ChromiumRE::Interfaces::Delegates::IClientDelegate {
     public:
        virtual ~BrowserWindow();

        /**
         * Create a new browser and native window.
         * @param $parentHandle Specify parent handle.
         * @param $rect Specify browser area.
         * @param $settings Specify browser settings.
         * @param $url Specify startup URL.
         * @param $requestContext Specify request context or null to use global context.
         */
        virtual void CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect,
                                   const CefBrowserSettings &$settings, const CefString &$url,
                                   CefRefPtr<CefRequestContext> $requestContext) = 0;

        /**
         * Show the window.
         */
        virtual void Show() = 0;

        /**
         * Hide the window.
         */
        virtual void Hide() = 0;

        /**
         * Set the window bounds in parent coordinates.
         * @param $x
         * @param $y
         * @param $width
         * @param $height
         */
        virtual void SetBounds(int $x, int $y, size_t $width, size_t $height) = 0;

        /**
         * Set focus to the window.
         * @param $focus True for focus, false for blur.
         */
        void SetFocus(bool $focus);

        /**
         * @return Returns the window handle.
         */
        virtual ClientWindowHandle GetWindowHandle() const = 0;

        /**
         * @return Returns the browser owned by the window.
         */
        CefRefPtr<CefBrowser> GetBrowser() const;

        /**
         * @return Returns true if the browser is closing.
         */
        bool IsClosing() const;

        /**
         * Set hidden attribute.
         * @param $value
         */
        void setIsHidden(bool $value) {
            this->isHidden = $value;
        }

        CefRefPtr<ClientHandler> getClientHander() {
            return this->clientHandler;
        }

     protected:
        friend struct base::DefaultDeleter<BrowserWindow>;

        explicit BrowserWindow(Io::Oidis::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate *$delegate);

        void OnBrowserCreated(CefRefPtr<CefBrowser> $browser) override;

        void OnBrowserClosing(CefRefPtr<CefBrowser> $browser) override;

        void OnBrowserClosed(CefRefPtr<CefBrowser> $browser) override;

        void OnSetAddress(const string &$url) override;

        void OnSetTitle(const string &$title) override;

        void OnSetFullscreen(bool $fullscreen) override;

        void OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) override;

        void OnSetDraggableRegions(const std::vector<CefDraggableRegion> &$regions) override;

        Io::Oidis::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate *delegate;
        CefRefPtr<CefBrowser> browser;
        CefRefPtr<Io::Oidis::ChromiumRE::Browser::ClientHandler> clientHandler;
        bool isClosing;
        bool isHidden = false;

     private:
        DISALLOW_COPY_AND_ASSIGN(BrowserWindow);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_BROWSERWINDOW_HPP_
