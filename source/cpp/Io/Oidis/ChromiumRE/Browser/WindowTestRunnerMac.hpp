/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERMAC_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERMAC_HPP_

#ifdef MAC_PLATFORM

namespace Io::Oidis::ChromiumRE::Browser {
    /**
     * OS X platform implementation. Methods are safe to call on any browser
     * process thread.
     */
    class WindowTestRunnerMac : public Io::Oidis::ChromiumRE::Browser::WindowTestRunner {
    };
}

#endif  // MAC_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERMAC_HPP_
