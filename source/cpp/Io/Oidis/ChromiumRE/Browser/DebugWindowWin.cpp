/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    using Io::Oidis::ChromiumRE::Connectors::WindowHandler;
    using Io::Oidis::ChromiumRE::Interfaces::Delegates::IWindowDelegate;
    using Io::Oidis::XCppCommons::Utils::LogIt;
    using Io::Oidis::ChromiumRE::Connectors::QueryResponse;
    using Io::Oidis::ChromiumRE::Enums::WindowStateType;

    namespace UtilWin = Io::Oidis::ChromiumRE::Commons::UtilWin;

    void DebugWindow::Close(bool $force) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            if ($force) {
                DestroyWindow(this->handle);
            } else {
                PostMessage(this->handle, WM_CLOSE, 0, 0);
            }
        }
    }

    void DebugWindow::CreateBrowserWindow(const string &$url) {
        this->browserWindow.reset(new BrowserWindowStdWin(this, $url));
    }

    void DebugWindow::CreateRootWindow(const CefBrowserSettings &$settings) {
        REQUIRE_MAIN_THREAD();
        DCHECK(!this->handle);

        HINSTANCE hInstance = GetModuleHandle(nullptr);
        const std::wstring windowTitle = DebugWindow::getWindowTypeName();
        const std::wstring windowClass = DebugWindow::getWindowTypeName();

        const cef_color_t backgroundColor = MainContext::Get()->getBackgroundColor();
        HBRUSH backgroundBrush = CreateSolidBrush(
                RGB(CefColorGetR(backgroundColor),
                    CefColorGetG(backgroundColor),
                    CefColorGetB(backgroundColor)));

        RegisterRootClass(hInstance, windowClass, backgroundBrush);

        this->findMessageId = RegisterWindowMessage(FINDMSGSTRING);
        CHECK(this->findMessageId);

        EnumWindows(EnumWindowsProc, 0);

        this->handle = CreateWindow(windowClass.c_str(), windowTitle.c_str(), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
                                  CW_USEDEFAULT, CW_USEDEFAULT, 1024, 780,
                                  nullptr, nullptr, hInstance, nullptr);

        SetWindowLongPtr(this->handle, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));

        UtilWin::SetUserDataPtr(this->handle, this);

        RECT rect{};
        GetClientRect(this->handle, &rect);

        CefRect cefRect(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);

        this->browserWindow->CreateBrowser(
                this->handle, cefRect, $settings,
                "http://localhost:" + std::to_string(
                        Io::Oidis::ChromiumRE::Commons::Configuration::getInstance().getRemoteDebuggingPort()),
                delegate->GetRequestContext(this));
        this->browserWindow->Show();

        ShowWindow(this->handle, SW_SHOWNORMAL);
    }

    void DebugWindow::RegisterRootClass(HINSTANCE $hInstance, const std::wstring &$windowClass, HBRUSH $backgroundBrush) {
        static bool classRegistered = false;
        if (classRegistered) {
            return;
        }
        classRegistered = true;

        WNDCLASSEX wcex{};

        wcex.cbSize = sizeof(WNDCLASSEX);

        wcex.style = CS_HREDRAW | CS_VREDRAW;
        wcex.lpfnWndProc = RootWndProc;
        wcex.cbClsExtra = 0;
        wcex.cbWndExtra = 0;
        wcex.hInstance = $hInstance;
        wcex.hIcon = LoadIcon($hInstance, MAKEINTRESOURCE(0));
        wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
        wcex.hbrBackground = $backgroundBrush;
        wcex.lpszMenuName = nullptr;
        wcex.lpszClassName = $windowClass.c_str();
        wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(0));

        RegisterClassEx(&wcex);
    }

    void CALLBACK DebugWindow::DelayedSingleClick(HWND $hwnd, UINT /*unused*/, UINT_PTR $id, DWORD /*unused*/) {
        REQUIRE_MAIN_THREAD();

        DCHECK($hwnd == UtilWin::GetUserDataPtr<DebugWindow *>($hwnd)->handle);

        KillTimer($hwnd, $id);
        WindowHandler::FireEvent("OnNotifyIconClick");
    }

    LRESULT CALLBACK DebugWindow::RootWndProc(HWND $hWnd, UINT $message,
                                              WPARAM $wParam, LPARAM $lParam) {
        REQUIRE_MAIN_THREAD();

        DebugWindow *self = UtilWin::GetUserDataPtr<DebugWindow *>($hWnd);
        if (self == nullptr) {
            return DefWindowProc($hWnd, $message, $wParam, $lParam);
        }
        DCHECK($hWnd == self->handle);

        if ($message == self->findMessageId) {
            auto lpfr = reinterpret_cast<LPFINDREPLACE>($lParam);
            CHECK(lpfr == &self->findState);
            self->OnFindEvent();
            return 0;
        }

        switch ($message) {
            case WM_PAINT: {
                self->OnPaint();
                return 0;
            }
            case WM_SETFOCUS: {
                self->OnFocus();
                return 0;
            }
            case WM_SIZE: {
                self->OnSize($wParam == SIZE_MINIMIZED);
                break;
            }
            case WM_MOVING:
            case WM_MOVE: {
                self->OnMove();
                return 0;
            }
            case WM_ERASEBKGND: {
                if (self->OnEraseBkgnd())
                    break;
                return 0;
            }
            case WM_CLOSE: {
                if (self->OnClose()) {
                    return 0;
                }
                break;
            }
            case WM_NCDESTROY: {
                UtilWin::SetUserDataPtr($hWnd, nullptr);
                self->handle = nullptr;
                self->OnDestroyed();
                return 0;
            }
            default:
                break;
        }

        return DefWindowProc($hWnd, $message, $wParam, $lParam);
    }

    void DebugWindow::OnPaint() {
        PAINTSTRUCT ps{};
        BeginPaint(this->handle, &ps);
        EndPaint(this->handle, &ps);
    }

    void DebugWindow::OnFocus() {
        if (this->browserWindow) {
            this->browserWindow->SetFocus(true);
        }
    }

    void DebugWindow::OnSize(bool $minimized) {
        if ($minimized) {
            if (this->browserWindow) {
                this->browserWindow->Hide();
            }
            return;
        }

        if (this->browserWindow) {
            this->browserWindow->Show();
        }

        RECT rect{};
        GetClientRect(this->handle, &rect);
        this->browserWindow->SetBounds(0, 0, (size_t)rect.right, (size_t)rect.bottom);
    }

    void DebugWindow::OnMove() {
        CefRefPtr<CefBrowser> browser = getBrowser();
        if (browser) {
            browser->GetHost()->NotifyMoveOrResizeStarted();
        }
    }

    bool DebugWindow::OnEraseBkgnd() {
        return (getBrowser() == nullptr);
    }

    void DebugWindow::OnFindEvent() {
        CefRefPtr<CefBrowser> browser = getBrowser();

        if ((this->findState.Flags & FR_DIALOGTERM) == FALSE) {
            if (browser) {
                browser->GetHost()->StopFinding(true);
                this->findWhatLast.clear();
                this->findNext = false;
            }
        } else if ((this->findState.Flags & FR_FINDNEXT) && browser) {
            bool matchCase = (this->findState.Flags & FR_MATCHCASE) != 0;
            const std::wstring &findWhat = this->findBuff;
            if (matchCase != this->findMatchCaseLast || findWhat != this->findWhatLast) {
                if (!findWhat.empty()) {
                    browser->GetHost()->StopFinding(true);
                    this->findNext = false;
                }
                this->findMatchCaseLast = matchCase;
                this->findWhatLast = this->findBuff;
            }

            browser->GetHost()->Find(0, findWhat, (this->findState.Flags & FR_DOWN) != 0, matchCase, this->findNext);
            if (!this->findNext) {
                this->findNext = true;
            }
        }
    }

    bool DebugWindow::OnClose() {
        bool retVal = false;
        if (this->browserWindow && !this->browserWindow->IsClosing()) {
            CefRefPtr<CefBrowser> browser = getBrowser();
            if (browser) {
                browser->GetHost()->CloseBrowser(false);
                retVal = true;
            }
        }

        if (boost::iequals(this->id, "root")) {
            if (this->notifyIcon) {
                this->notifyIcon->Destroy();
            }
            MainContext::Get()->getRootWindowManager()->CloseAllWindows(true);
        }

        return retVal;
    }

    void DebugWindow::OnDestroyed() {
        this->windowDestroyed = true;
        this->notifyDestroyedIfDone();
    }

    void DebugWindow::OnBrowserCreated(CefRefPtr<CefBrowser> $browser) {
        REQUIRE_MAIN_THREAD();
        (void)$browser;

        OnSize(false);
    }

    void DebugWindow::OnBrowserWindowDestroyed() {
        REQUIRE_MAIN_THREAD();

        this->browserWindow.reset();

        if (!this->windowDestroyed) {
            Close(true);
        }

        this->browserDestroyed = true;
        this->notifyDestroyedIfDone();
    }

    void DebugWindow::OnSetTitle(const string &$title) {
        REQUIRE_MAIN_THREAD();

        if (this->handle != nullptr) {
            SetWindowText(this->handle, CefString($title).ToWString().c_str());
        }
    }

    void DebugWindow::Show(WindowStateType $mode) {}

    void DebugWindow::Hide() {}

    void DebugWindow::SetBounds(int $x, int $y, size_t $width, size_t $height) {}

    WindowStateType DebugWindow::getWindowState() const {
        return WindowStateType::NORMAL;
    }

    void DebugWindow::ExecuteScript(const json &$options, const shared_ptr<QueryResponse> $response) {}

    void DebugWindow::SendScriptResponse(const string &$response) const {}

    bool DebugWindow::CreateNotifyIcon(std::shared_ptr<NotifyIconOptions> $options,
                                       const shared_ptr<QueryResponse> $response) {
        return false;
    }

    bool DebugWindow::ModifyNotifyIcon(std::shared_ptr<NotifyIconOptions> $options,
                                       const shared_ptr<QueryResponse> $response) {
        return false;
    }

    bool DebugWindow::DestroyNotifyIcon(const shared_ptr<QueryResponse> $response) {
        return false;
    }

    void DebugWindow::Minimize() {}

    void DebugWindow::Maximize() {}

    void DebugWindow::Restore() {}

    void DebugWindow::OnSetLoadingState(bool $isLoading, bool $canGoBack, bool $canGoForward) {}
}

#endif  // WIN_PLATFORM
