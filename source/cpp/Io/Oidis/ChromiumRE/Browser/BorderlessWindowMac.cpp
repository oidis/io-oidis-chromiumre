/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include "../sourceFilesMap.hpp"

@implementation BorderlessWindow

- (id)Initialize:(NSRect)$rectangle {
    if (self = [super initWithContentRect:$rectangle
                      styleMask:NSWindowStyleMaskBorderless | NSWindowStyleMaskMiniaturizable
                      backing:NSBackingStoreBuffered
                      defer:NO]) {
        self.maximized = NO;
        self.releasedWhenClosed = NO;
    }

    return self;
}

- (void)Maximize {
    [self toogleZoom];

    self.maximized = YES;
}

- (void)Restore {
    [self toogleZoom];

    self.maximized = NO;
}

- (void)toogleZoom {
    self.styleMask |= NSWindowStyleMaskResizable;

    [self zoom:nil];

    self.styleMask &= ~NSWindowStyleMaskResizable;
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}

- (void)performClose:(id)__unused sender {
    if ([self windowShouldClose:self]) {
        [self close];
    }
}

- (BOOL)windowShouldClose:(id)__unused window {
    return YES;
}

@end;

#endif  // MAC_PLATFORM
