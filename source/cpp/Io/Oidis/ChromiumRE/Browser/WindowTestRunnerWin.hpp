/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERWIN_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERWIN_HPP_

#ifdef WIN_PLATFORM

namespace Io::Oidis::ChromiumRE::Browser {
    /**
     * Windows platform implementation. Methods are safe to call on any browser
     * process thread.
     */
    class WindowTestRunnerWin : public Io::Oidis::ChromiumRE::Browser::WindowTestRunner {
    };
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_WINDOWTESTRUNNERWIN_HPP_
