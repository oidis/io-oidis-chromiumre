/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_BROWSER_DIALOGHANDLERLINUX_HPP_
#define IO_OIDIS_CHROMIUMRE_BROWSER_DIALOGHANDLERLINUX_HPP_

#ifdef LINUX_PLATFORM

namespace Io::Oidis::ChromiumRE::Browser {
    class DialogHandlerLinux : public CefDialogHandler {
     public:
        DialogHandlerLinux() = default;

        bool OnFileDialog(CefRefPtr<CefBrowser> $browser,
                          FileDialogMode $mode,
                          const CefString &$title,
                          const CefString &$defaultFilePath,
                          const std::vector<CefString> &$acceptFilters,
                          int $selectedAcceptFilter,
                          CefRefPtr<CefFileDialogCallback> $callback) override;

     private:
        struct OnFileDialogParams {
            CefRefPtr<CefBrowser> browser = nullptr;
            FileDialogMode mode = cef_file_dialog_mode_t::FILE_DIALOG_OPEN;
            CefString title;
            CefString defaultFilePath;
            std::vector<CefString> acceptFilters;
            int selectedAcceptFilter = 0;
            CefRefPtr<CefFileDialogCallback> callback = nullptr;
        };

        void onFileDialogContinue(OnFileDialogParams $params, GtkWindow *$window);

        void getWindowAndContinue(CefRefPtr<CefBrowser> $browser, base::Callback<void(GtkWindow *)> $callback);

        GtkWidget *dialog = nullptr;

        IMPLEMENT_REFCOUNTING(DialogHandlerLinux);
        DISALLOW_COPY_AND_ASSIGN(DialogHandlerLinux);
    };
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_BROWSER_DIALOGHANDLERLINUX_HPP_
