/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <gdk/gdk.h>

#include "../sourceFilesMap.hpp"

#include <gdk/gdkx.h>  // NOLINT
#include <X11/Xlib.h>  // NOLINT

using Io::Oidis::ChromiumRE::Interfaces::Delegates::IBrowserWindowDelegate;
using Io::Oidis::XCppCommons::Utils::LogIt;

namespace {
    Window GetXWindowForWidget(GtkWidget *$widget) {
        CHECK($widget != 0);  // NOLINT

        Window xwindow = GDK_WINDOW_XID(gtk_widget_get_window($widget));
        DCHECK(xwindow);
        return xwindow;
    }

    void SetXWindowVisible(XDisplay *$xdisplay, Window $xwindow, bool $visible) {
        CHECK($xdisplay != 0);  // NOLINT
        CHECK($xwindow != 0);  // NOLINT

        const char *kAtoms[] = {"_NET_WM_STATE", "ATOM", "_NET_WM_STATE_HIDDEN"};
        Atom atoms[3] = { 0 };
        const int result = XInternAtoms($xdisplay, const_cast<char **>(kAtoms), 3, false, atoms);
        if (!result) {
            NOTREACHED();
        }

        if (!$visible) {
            scoped_ptr<Atom[]> data(new Atom[1]);
            data[0] = atoms[2];

            XChangeProperty($xdisplay, $xwindow,
                    atoms[0],
                    atoms[1],
                    32,
                    PropModeReplace,
                    reinterpret_cast<const unsigned char*>(data.get()),
                    1);
        } else {
            XChangeProperty($xdisplay, $xwindow,
                    atoms[0],
                    atoms[1],
                    32,
                    PropModeReplace, NULL,
                    0);
        }
    }

    void SetXWindowBounds(XDisplay *$xdisplay, Window $xwindow,
                      int $x,
                      int $y,
                      size_t $width,
                      size_t $height) {
        CHECK($xdisplay != 0);  // NOLINT
        CHECK($xwindow != 0);  // NOLINT

        XWindowChanges changes = { 0 };
        changes.x = $x;
        changes.y = $y;
        changes.width = static_cast<int>($width);
        changes.height = static_cast<int>($height);

        XConfigureWindow($xdisplay, $xwindow, CWX | CWY | CWHeight | CWWidth, &changes);
    }
}

namespace Io::Oidis::ChromiumRE::Browser {
    BrowserWindowStdLinux::BrowserWindowStdLinux(IBrowserWindowDelegate *$delegate, const string &$startupUrl)
            : BrowserWindow($delegate) {
        this->clientHandler = new ClientHandlerStdLinux(this, $startupUrl);
    }

    void BrowserWindowStdLinux::SetXdisplay(XDisplay *$xdisplay) {
        REQUIRE_MAIN_THREAD();

        DCHECK(!this->xdisplay);

        this->xdisplay = $xdisplay;
    }

    void BrowserWindowStdLinux::CreateBrowser(ClientWindowHandle $parentHandle, const CefRect &$rect,
                                            const CefBrowserSettings &$settings, const CefString &$url,
                                            CefRefPtr<CefRequestContext> $requestContext) {
        REQUIRE_MAIN_THREAD();

        CefWindowInfo windowInfo;
        windowInfo.SetAsChild(GetXWindowForWidget($parentHandle), $rect);

        CefBrowserHost::CreateBrowser(std::move(windowInfo), this->clientHandler, $url, $settings, nullptr, $requestContext);
    }

    void BrowserWindowStdLinux::Show() {
        REQUIRE_MAIN_THREAD();

        if (!this->isHidden && this->browser != nullptr) {
            const Window xwindow = this->browser->GetHost()->GetWindowHandle();
            if (xwindow && this->xdisplay != nullptr) {
                SetXWindowVisible(this->xdisplay, xwindow, true);
            }
        }
    }

    void BrowserWindowStdLinux::Hide() {
        REQUIRE_MAIN_THREAD();

        if (!this->isHidden && this->browser != nullptr) {
            const Window xwindow = this->browser->GetHost()->GetWindowHandle();
            if (xwindow && this->xdisplay != nullptr) {
                SetXWindowVisible(this->xdisplay, xwindow, false);
            }
        }
    }

    void BrowserWindowStdLinux::SetBounds(int $x, int $y, size_t $width, size_t $height) {
        REQUIRE_MAIN_THREAD();

        if (this->browser != nullptr) {
            const Window xwindow = this->browser->GetHost()->GetWindowHandle();
            if (xwindow && this->xdisplay != nullptr) {
                SetXWindowBounds(this->xdisplay, xwindow, $x, $y, $width, $height);
            }
        }
    }

    ClientWindowHandle BrowserWindowStdLinux::GetWindowHandle() const {
        REQUIRE_MAIN_THREAD();

        return nullptr;
    }
}

#endif  // LINUX_PLATFORM
