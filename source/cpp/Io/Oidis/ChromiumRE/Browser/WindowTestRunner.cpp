/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    using Io::Oidis::ChromiumRE::Commons::Configuration;
    using Io::Oidis::ChromiumRE::Browser::MainContext;
    using Io::Oidis::ChromiumRE::Browser::RootWindow;
    using Io::Oidis::ChromiumRE::Connectors::QueryResponse;
    using Io::Oidis::ChromiumRE::Commons::WaitableEvent;
    using Io::Oidis::ChromiumRE::Commons::CookieVisitor;
    using Io::Oidis::ChromiumRE::Commons::DialogCallback;
    using Io::Oidis::ChromiumRE::Connectors::Detail::NotifyIconOptionsJsonProxy;
    using Io::Oidis::XCppCommons::Primitives::String;
    using Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconOptions;

    class ParameterWrapper {
     public:
        ParameterWrapper(const shared_ptr<QueryResponse> $response,
                         const string $url)
                : response($response),
                  url($url) {}

        const std::function<void()> getOnCookiesCallback() const {
            return this->onCookiesLoadedCallback;
        }

        std::vector<CefCookie> *getCookies() {
            return &this->cookies;
        }

     private:
        std::vector<CefCookie> cookies;
        shared_ptr<QueryResponse> response = nullptr;
        string url;

        const std::function<void()> onCookiesLoadedCallback = [this]() {
            json ret = json::array();
            for (size_t i = 0; i < this->cookies.size(); i++) {
                CefCookie *cookie = &this->cookies.at(i);
                if (String::Contains(CefString(&cookie->domain).ToString(), this->url)) {
                    ret.insert(ret.end(), json({
                                                       {"name",  CefString(&cookie->name).ToString()},
                                                       {"value", CefString(&cookie->value).ToString()},
                                                       {"path",  CefString(&cookie->path).ToString()}
                                               }));
                }
            }

            if (this->response) {
                this->response->Send(ret);
            }

            // Cookies are read and sent to frontend, now just delete the object to free memory.
            delete this;
        };
    };

    void WindowTestRunner::MoveTo(CefRefPtr<CefBrowser> $browser, int $x, int $y, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$x, $y](scoped_refptr<RootWindow> rootWindow) {
            const auto bounds = WindowTestRunner::getRectangleForMove(rootWindow->getWindowHandle(), $x, $y);

            rootWindow->SetBounds(bounds.x, bounds.y, bounds.width, bounds.height);

            return true;
        });
    }

    void WindowTestRunner::Minimize(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Minimize();

            return true;
        });
    }

    void WindowTestRunner::Maximize(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Maximize();

            return true;
        });
    }

    void WindowTestRunner::Restore(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Restore();

            return true;
        });
    }

    void WindowTestRunner::Resize(CefRefPtr<CefBrowser> $browser, WindowCornerType $fixedCorner, int $dx, int $dy,
                                  const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$dx, $dy, $fixedCorner](scoped_refptr<RootWindow> rootWindow) {
            const auto bounds = WindowTestRunner::getRectangleForResize(rootWindow->getWindowHandle(), $dx, $dy, $fixedCorner);

            rootWindow->SetBounds(bounds.x, bounds.y, bounds.width, bounds.height);

            return true;
        });
    }

    void WindowTestRunner::CanResize(CefRefPtr<CefBrowser> $browser, bool $canResize, const shared_ptr<QueryResponse> $response) {
        Configuration::getInstance().setCanResize($canResize);

        if ($response != nullptr) {
            $response->Send(true);
        }
    }

    void WindowTestRunner::getWindowState(CefRefPtr<CefBrowser> $browser, const string &$window,
                                          const shared_ptr<QueryResponse> $response) {
        const string id = $window.empty() ? "root" : $window;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById(id);

        if (rootWindow != nullptr && $response != nullptr) {
            $response->Send(rootWindow->getWindowState().toString());
        }
    }

    void WindowTestRunner::Open(CefRefPtr<CefBrowser> $browser, const string &$url, const json &$options,
                                const shared_ptr<QueryResponse> $response) {
        MainContext::Get()->getRootWindowManager()->CreateRootWindow({}, $url, $options, $response);
    }

    void WindowTestRunner::ScriptExecute(CefRefPtr<CefBrowser> $browser, const string &$window, const json &$options,
                                         const shared_ptr<QueryResponse> $response) {
        const string id = $window.empty() ? "root" : $window;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById(id);

        if (rootWindow != nullptr) {
            rootWindow->ExecuteScript($options, $response);
        } else {
            $response->Send(false);
        }
    }

    void WindowTestRunner::Close(CefRefPtr<CefBrowser> $browser, const string &$window,
                                 const shared_ptr<QueryResponse> $response) {
        if ($window.empty()) {
            MainContext::Get()->getRootWindowManager()->CloseAllWindows(true);

            // TODO(nxf45876): Instead of calling the RootWindow::Close(), the RootWindowManager::Close() should exists, that would
            // perform some additional operations like destruction of window on OS X, but we don't have time now to change the architecture.
            // NotifyDestroyIfDone() is not called on OS X due to the bug in single-process mode on OS X
            WindowTestRunner::performOnCloseActions(MainContext::Get()->getRootWindowManager()->getWindowById("root"));
        } else {
            bool status = false;
            const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById($window);
            if (rootWindow != nullptr) {
                rootWindow->Close(true);
                WindowTestRunner::performOnCloseActions(rootWindow);

                status = true;
            }

            if ($response != nullptr) {
                $response->Send(status);
            }
        }
    }

    void WindowTestRunner::getCookies(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response) {
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById($window);

        if (rootWindow != nullptr) {
            $browser = rootWindow->getBrowser();
        }

        CefRefPtr<CefCookieManager> manager = CefCookieManager::GetGlobalManager(nullptr);

        string url = $browser->GetMainFrame()->GetURL().ToString();
        if (!String::StartsWith(url, "file://")) {
            url = String::Remove(url, "http://");
            url = String::Remove(url, "https://");
            url = String::Remove(url, "www.");
            unsigned int index = url.find("/");
            if (index > 0) {
                url = url.substr(0, index);
            }
        } else {
            url = "chromiumre.wuiframework.com";
        }

        // This object is created on purpose, because VisitAllCookies is asynchronous and
        // it holds url, response and cookies vector even when this function is cleaned.
        auto *pWrap = new ParameterWrapper($response, url);
        manager->VisitAllCookies(new CookieVisitor(pWrap->getOnCookiesCallback(), pWrap->getCookies()));
    }

    void WindowTestRunner::Show(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Show(WindowStateType::NORMAL);

            return true;
        });
    }

    void WindowTestRunner::Hide(CefRefPtr<CefBrowser> $browser, const string &$window, const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [](scoped_refptr<RootWindow> rootWindow) {
            rootWindow->Hide();

            return true;
        });
    }

    void WindowTestRunner::CreateNotifyIcon(CefRefPtr<CefBrowser> $browser, const json &$options,
                                            const shared_ptr<QueryResponse> $response) {
        auto iconOptions = std::make_shared<NotifyIconOptions>();
        NotifyIconOptionsJsonProxy::Update($options, iconOptions);

        const bool status = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->CreateNotifyIcon(
                iconOptions, $response);

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::ModifyNotifyIcon(CefRefPtr<CefBrowser> $browser, const json &$options,
                                            const shared_ptr<QueryResponse> $response) {
        auto iconOptions = std::make_shared<NotifyIconOptions>();
        NotifyIconOptionsJsonProxy::Update($options, iconOptions);

        const bool status = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->ModifyNotifyIcon(
                iconOptions, $response);

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::DestroyNotifyIcon(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response) {
        const bool status = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier())->
                DestroyNotifyIcon($response);

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::setTaskBarProgressState(CefRefPtr<CefBrowser> $browser,
                                                   TaskBarBase::TaskbarProgressState $state,
                                                   const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$state](scoped_refptr<RootWindow> rootWindow) {
            WindowTestRunner::createTaskBarIfNotExists(rootWindow);

            if (rootWindow->getTaskBar() != nullptr) {
                return rootWindow->getTaskBar()->setProgressState($state);
            }

            return false;
        });
    }

    void WindowTestRunner::setTaskBarProgressValue(CefRefPtr<CefBrowser> $browser, const int $completed, const int $total,
                                                   const shared_ptr<QueryResponse> $response) {
        WindowTestRunner::performAction($browser, $response, [$completed, $total](scoped_refptr<RootWindow> rootWindow) {
            WindowTestRunner::createTaskBarIfNotExists(rootWindow);

            if (rootWindow->getTaskBar() != nullptr) {
                return rootWindow->getTaskBar()->setProgressValue($completed, $total);
            }

            return false;
        });
    }

    void WindowTestRunner::ShowFileDialog(CefRefPtr<CefBrowser> $browser, const json &$settings,
                                          const shared_ptr<QueryResponse> $response) {
        Io::Oidis::ChromiumRE::Connectors::Detail::WindowHandlerFileDialog fileDialog;
        fileDialog.Update($settings);

        cef_file_dialog_mode_t dialogMode;

        if (fileDialog.isFolderOnly()) {
            dialogMode = CefBrowserHost::FileDialogMode::FILE_DIALOG_OPEN_FOLDER;
        } else {
            if (fileDialog.isMultiSelect()) {
                dialogMode = CefBrowserHost::FileDialogMode::FILE_DIALOG_OPEN_MULTIPLE;
            } else if (fileDialog.isOpenOnly()) {
                dialogMode = CefBrowserHost::FileDialogMode::FILE_DIALOG_OPEN;
            } else {
                dialogMode = static_cast<cef_file_dialog_mode_t>(
                        CefBrowserHost::FileDialogMode::FILE_DIALOG_SAVE |
                        CefBrowserHost::FileDialogMode::FILE_DIALOG_OVERWRITEPROMPT_FLAG);
            }
        }

        std::vector<CefString> acceptFilters;
        std::for_each(fileDialog.getFilter().begin(), fileDialog.getFilter().end(), [&](const string &$item) {
            acceptFilters.push_back($item);
        });
        string initDir = fileDialog.getInitialDirectory();
        using Io::Oidis::XCppCommons::System::IO::FileSystem;

        if (!initDir.empty() && !FileSystem::Exists(initDir)) {
#ifdef WIN_PLATFORM
            char *buff = getenv("HOMEPATH");
            char *buff2 = getenv("HOMEDRIVE");
            if (buff != nullptr && buff2 != nullptr) {
                initDir = string(buff2) + string(buff) + "\\";
            }
            if (!FileSystem::Exists(initDir)) {
                initDir = FileSystem::getTempPath() + "\\";
            }
#else
            char *buff = getenv("HOME");
            if (buff != nullptr) {
                initDir = string(buff);
            }
            if (!FileSystem::Exists(initDir)) {
                initDir = FileSystem::getTempPath() + "/";
            }
#endif
        }

        boost::filesystem::path filePath(fileDialog.getPath());
        if (!filePath.empty()) {
            if (filePath.is_absolute()) {
                if (FileSystem::Exists(filePath.string())) {
                    initDir = filePath.string();
                } else {
                    initDir += filePath.filename().string();
                }
            } else {
                initDir += fileDialog.getPath();
            }
        }

        boost::filesystem::path initDirPath(initDir);
        $browser->GetHost()->RunFileDialog(dialogMode, fileDialog.getTitle(),
                                           initDirPath.normalize().make_preferred().string(),
                                           acceptFilters, fileDialog.getFilterIndex(),
                                           new DialogCallback($response));
    }

    void WindowTestRunner::ShowDebugConsole(CefRefPtr<CefBrowser> $browser, const shared_ptr<QueryResponse> $response) {
        bool status = false;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->getWindowById("root");

        if (Io::Oidis::ChromiumRE::Commons::Configuration::getInstance().getRemoteDebuggingPort() > 0) {
            MainContext::Get()->getRootWindowManager()->CreateDebugWindow($browser);
            status = true;
        }

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::performAction(CefRefPtr<CefBrowser> $browser, shared_ptr<QueryResponse> $response,
                                         std::function<bool(scoped_refptr<RootWindow>)> $callback) {
        bool status = false;
        const auto rootWindow = MainContext::Get()->getRootWindowManager()->GetWindowForBrowser($browser->GetIdentifier());

        if (rootWindow != nullptr) {
            const auto windowHandle = rootWindow->getWindowHandle();

            if (windowHandle != nullptr) {
                status = $callback(rootWindow);
            }
        }

        if ($response != nullptr) {
            $response->Send(status);
        }
    }

    void WindowTestRunner::createTaskBarIfNotExists(scoped_refptr<RootWindow> $rootWindow) {
        if (!$rootWindow->getTaskBar()) {
            shared_ptr<TaskBarBase> taskBar(Io::Oidis::Onion::Gui::TaskBar::TaskBarFactory::Create(
                    static_cast<void *>($rootWindow->getWindowHandle())));
            $rootWindow->setTaskBar(taskBar);
        }
    }
}
