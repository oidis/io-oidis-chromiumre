/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

#include <shlobj.h>  // NOLINT

namespace Io::Oidis::ChromiumRE::Browser {
    string MainContextImpl::getDownloadPath(const string &$fileName) {
        TCHAR szFolderPath[MAX_PATH];
        string path;

        if (SUCCEEDED(SHGetFolderPath(nullptr, CSIDL_PERSONAL | CSIDL_FLAG_CREATE,
                                      nullptr, 0, szFolderPath))) {
            path = CefString(szFolderPath);
            path += "\\" + $fileName;
        }

        return path;
    }

    string MainContextImpl::getAppWorkingDirectory() {
        char szWorkingDir[MAX_PATH + 1];
        if (_getcwd(szWorkingDir, MAX_PATH) == nullptr) {
            szWorkingDir[0] = 0;
        } else {
            size_t len = strlen(szWorkingDir);
            szWorkingDir[len] = '\\';
            szWorkingDir[len + 1] = 0;
        }
        return string(szWorkingDir);
    }
}

#endif  // WIN_PLATFORM
