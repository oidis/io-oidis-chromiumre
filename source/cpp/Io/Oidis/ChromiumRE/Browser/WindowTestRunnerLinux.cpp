/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    using Io::Oidis::ChromiumRE::Enums::WindowStateType;

    CefRect WindowTestRunner::getRectangleForMove(ClientWindowHandle $handle, int $x, int $y) {
        GtkAllocation allocation = { 0 };
        gtk_widget_get_allocation($handle, &allocation);

        return { $x, $y, allocation.width, allocation.height };
    }

    CefRect WindowTestRunner::getRectangleForResize(ClientWindowHandle $handle, int $dx, int $dy, WindowCornerType $fixedCorner) {
        GtkAllocation allocation = { 0 };
        gtk_widget_get_allocation($handle, &allocation);

        gint x = 0;
        gint y = 0;
        gdk_window_get_origin(gtk_widget_get_window($handle), &x, &y);

        switch ($fixedCorner) {
            case WindowCornerType::TOP_LEFT:
                allocation.width += $dx;
                allocation.height += $dy;

                break;
            case WindowCornerType::TOP_RIGHT:
                allocation.x -= $dx;
                allocation.width += $dx;
                allocation.height += $dy;

                break;
            case WindowCornerType::BOTTOM_LEFT:
                allocation.y -= $dy;
                allocation.width += $dx;
                allocation.height += $dy;

                break;
            case WindowCornerType::BOTTOM_RIGHT:
                allocation.x += $dx;
                allocation.y += $dy;
                allocation.width -= $dx;
                allocation.height -= $dy;

                break;
        }

        return { allocation.x + x, allocation.y + y, allocation.width, allocation.height };
    }

    void WindowTestRunner::performOnCloseActions(RootWindow *$rootWindow) {
    }
}

#endif  // LINUX_PLATFORM
