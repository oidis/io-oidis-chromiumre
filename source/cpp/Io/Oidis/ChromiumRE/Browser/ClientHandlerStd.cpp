/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::ChromiumRE::Interfaces::Delegates::IClientDelegate;

namespace Io::Oidis::ChromiumRE::Browser {
    ClientHandlerStd::ClientHandlerStd(IClientDelegate *delegate, const string &startupUrl)
            : ClientHandler(delegate, startupUrl) {
    }
}
