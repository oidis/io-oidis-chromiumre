/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Browser {
    using Io::Oidis::ChromiumRE::Browser::RootWindow;
    using Io::Oidis::ChromiumRE::Connectors::QueryResponse;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    class ClientRequestContextHandler : public CefRequestContextHandler {
     public:
        ClientRequestContextHandler() {}

     private:
        IMPLEMENT_REFCOUNTING(ClientRequestContextHandler);
    };

    // cppcheck-suppress uninitMemberVar
    RootWindowManager::RootWindowManager(bool $terminateWhenAllWindowsClosed)
            : terminateWhenAllWindowsClosed($terminateWhenAllWindowsClosed) {
        CefRefPtr<CefCommandLine> commandLine = CefCommandLine::GetGlobalCommandLine();
        DCHECK(commandLine.get());
    }

    scoped_refptr<RootWindow> RootWindowManager::CreateRootWindow(const ChromiumArgs &$args, const string &$url) {
        CefBrowserSettings settings;
        MainContext::Get()->PopulateBrowserSettings(&settings);
        scoped_refptr<RootWindow> rootWindow = RootWindow::Create(MainContext::Get()->UseViews());
        rootWindow->Init(this, $args, settings, $url);

        OnRootWindowCreated(rootWindow);

        LogIt::Info("Creating ROOT window with URI {0}", $url);

        return rootWindow;
    }

    scoped_refptr<RootWindow> RootWindowManager::CreateRootWindow(const ChromiumArgs &$args, const string &$url, const json &$options,
                                                                  const shared_ptr<QueryResponse> $response) {
        CefBrowserSettings settings;
        MainContext::Get()->PopulateBrowserSettings(&settings);

        scoped_refptr<RootWindow> rootWindow = RootWindow::Create(MainContext::Get()->UseViews());
        rootWindow->Init(this, $args, settings, $url, $options, $response);

        OnRootWindowCreated(rootWindow);

        LogIt::Info("Creating OTHER window.");

        return rootWindow;
    }

    void RootWindowManager::DestroyRootWindow(RootWindow *$rootWindow) {
        REQUIRE_MAIN_THREAD();

        RootWindowSet::iterator it = rootWindows.find($rootWindow);
        DCHECK(it != std::cend(rootWindows));
        if (it != std::cend(rootWindows)) {
            rootWindows.erase(it);
        }

        if (terminateWhenAllWindowsClosed && rootWindows.empty()) {
            MainMessageLoop::Get()->Quit();
        }
    }

    scoped_refptr<RootWindow> RootWindowManager::CreateDebugWindow(const CefRefPtr<CefBrowser> &$parent) {
#ifdef WIN_PLATFORM
        CefBrowserSettings settings;
        MainContext::Get()->PopulateBrowserSettings(&settings);

        scoped_refptr<RootWindow> rootWindow(new DebugWindow());
        rootWindow->Init(this, {}, settings, "");

        OnRootWindowCreated(rootWindow);

        LogIt::Info("Creating Debug-Console window.");

        return rootWindow;
#else
#warning "RootWindowManager::CreateDebugWindow is not implemented on this platform"
        return nullptr;
#endif
    }

    scoped_refptr<RootWindow> RootWindowManager::GetWindowForBrowser(int $browserId) {
        REQUIRE_MAIN_THREAD();

        RootWindowSet::const_iterator it = rootWindows.begin();
        for (; it != rootWindows.end(); ++it) {
            CefRefPtr<CefBrowser> browser = (*it)->getBrowser();
            if (browser.get() && browser->GetIdentifier() == $browserId) {
                return *it;
            }
        }
        return nullptr;
    }

    void RootWindowManager::CloseAllWindows(bool $force) {
        if (!CURRENTLY_ON_MAIN_THREAD()) {
            MAIN_POST_CLOSURE(base::Bind(&RootWindowManager::CloseAllWindows, base::Unretained(this), $force));
            return;
        }

        if (rootWindows.empty()) {
            return;
        }

        RootWindowSet::const_iterator it = rootWindows.begin();
        for (; it != rootWindows.end(); ++it) {
            (*it)->Close($force);
        }
    }

    void RootWindowManager::OnRootWindowCreated(scoped_refptr<RootWindow> $rootWindow) {
        if (!CURRENTLY_ON_MAIN_THREAD()) {
            MAIN_POST_CLOSURE(base::Bind(&RootWindowManager::OnRootWindowCreated, base::Unretained(this), $rootWindow));
            return;
        }

        rootWindows.insert($rootWindow);
    }

    CefRefPtr<CefRequestContext> RootWindowManager::GetRequestContext(RootWindow *$rootWindow) {
        REQUIRE_MAIN_THREAD();

        if (!sharedRequestContext.get()) {
            sharedRequestContext = CefRequestContext::CreateContext(CefRequestContext::GetGlobalContext(),
                                                                    new ClientRequestContextHandler());
        }
        return sharedRequestContext;
    }

    void RootWindowManager::OnRootWindowDestroyed(RootWindow *$rootWindow) {
        this->DestroyRootWindow($rootWindow);
    }

    scoped_refptr<RootWindow> RootWindowManager::getWindowById(const string &$id) const {
        REQUIRE_MAIN_THREAD();

        RootWindowSet::const_iterator it = rootWindows.begin();
        for (; it != rootWindows.end(); ++it) {
            if (boost::iequals(it->get()->getId(), $id)) {
                return *it;
            }
        }
        return nullptr;
    }
}
