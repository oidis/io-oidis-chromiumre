/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_CONNECTORS_QUERYRESPONSE_HPP_
#define IO_OIDIS_CHROMIUMRE_CONNECTORS_QUERYRESPONSE_HPP_

namespace Io::Oidis::ChromiumRE::Connectors {
    /**
     * QueryResponse class provides response API for communication over cef-query.
     * Only 'Success' method of cef-query::Callback is used. Error states are handled in QueryContent protocol.
     */
    class QueryResponse {
     public:
        /**
         * Constructs response from query and callback.
         * @param $query Specify QueryContentHandler.
         * @param $callback Specify cef-query callback.
         */
        explicit QueryResponse(const shared_ptr<QueryContentHandler> $query,
                               const CefRefPtr<CefMessageRouterBrowserSide::Callback> $callback);

        /**
         * Sends status with optional arguments.
         * @param $succeed Specify status.
         * @param $args Specify arguments in JSON, empty by default.
         */
        void Send(bool $succeed, const json &$args = json());

        /**
         * Sends string message with optional arguments.
         * @param $message Specify message.
         * @param $args Specify arguments in JSON, empty by default.
         */
        void Send(const string &$message, const json &$args = json());

        /**
         * Sends JSON data.
         * @param $args Specify JSON data to send.
         */
        void Send(const json &$args);

        /**
         * Sends exception.
         * @param $exception Specify exception.
         */
        void SendError(const std::exception &$exception);

        /**
         * Sends error message.
         * @param $message Specify message.
         */
        void SendError(const string &$message);

        /**
         * Sends data defined by JSON object.
         * @param $data Specify data.
         */
        void SendData(const json &$data);

        /**
         * Sends success state.
         * @param $succeed Set true to succeed, false otherwise.
         */
        void SendSuccess(bool $succeed);

        /**
         * @return Returns registered cef-query callback.
         */
        const CefRefPtr<CefMessageRouterBrowserSide::Callback> &getCallback() const;

        /**
         * @param $callback Register cef-query callback.
         */
        void setCallback(const CefRefPtr<CefMessageRouterBrowserSide::Callback> &$callback);

     private:
        shared_ptr<QueryContentHandler> query;
        CefRefPtr<CefMessageRouterBrowserSide::Callback> callback;

        void send(const json &$returnValue, const json &$args);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_CONNECTORS_QUERYRESPONSE_HPP_
