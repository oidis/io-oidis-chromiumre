/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Connectors::Detail {

    void WindowHandlerFileDialog::Update(const json &$options) {
        if (!$options.empty()) {
            if ($options.find("title") != $options.end()) {
                this->setTitle($options["title"]);
            }
            if ($options.find("filter") != $options.end()) {
                this->setFilter($options["filter"]);
            }
            if ($options.find("filterIndex") != $options.end()) {
                this->setFilterIndex($options["filterIndex"]);
            }
            if ($options.find("path") != $options.end()) {
                this->setPath($options["path"]);
            }
            if ($options.find("folderOnly") != $options.end()) {
                this->setFolderOnly($options["folderOnly"]);
            }
            if ($options.find("multiSelect") != $options.end()) {
                this->setMultiSelect($options["multiSelect"]);
            }
            if ($options.find("initialDirectory") != $options.end()) {
                this->setInitialDirectory($options["initialDirectory"]);
            }
            if ($options.find("openOnly") != $options.end()) {
                this->setOpenOnly($options["openOnly"]);
            }
        }
    }

    const string &WindowHandlerFileDialog::getTitle() const {
        return this->title;
    }

    void WindowHandlerFileDialog::setTitle(const string &$title) {
        this->title = $title;
    }

    const std::vector<string> &WindowHandlerFileDialog::getFilter() const {
        return this->filter;
    }

    void WindowHandlerFileDialog::setFilter(const std::vector<string> &$filter) {
        this->filter = $filter;
    }

    int WindowHandlerFileDialog::getFilterIndex() const {
        return this->filterIndex;
    }

    void WindowHandlerFileDialog::setFilterIndex(int $filterIndex) {
        this->filterIndex = $filterIndex;
    }

    const string &WindowHandlerFileDialog::getPath() const {
        return this->path;
    }

    void WindowHandlerFileDialog::setPath(const string &$path) {
        this->path = $path;
    }

    bool WindowHandlerFileDialog::isFolderOnly() const {
        return this->folderOnly;
    }

    void WindowHandlerFileDialog::setFolderOnly(bool $folderOnly) {
        this->folderOnly = $folderOnly;
    }

    bool WindowHandlerFileDialog::isMultiSelect() const {
        return this->multiSelect;
    }

    void WindowHandlerFileDialog::setMultiSelect(bool $multiSelect) {
        this->multiSelect = $multiSelect;
    }

    const string &WindowHandlerFileDialog::getInitialDirectory() const {
        return this->initialDirectory;
    }

    void WindowHandlerFileDialog::setInitialDirectory(const string &$initialDirectory) {
        this->initialDirectory = $initialDirectory;
    }

    bool WindowHandlerFileDialog::isOpenOnly() const {
        return this->openOnly;
    }

    void WindowHandlerFileDialog::setOpenOnly(bool $openOnly) {
        this->openOnly = $openOnly;
    }
}
