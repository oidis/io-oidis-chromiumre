/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Io::Oidis::ChromiumRE::Connectors::Detail::ContextMenuItemJsonProxy;

void ContextMenuItemJsonProxy::Update(const json &$options, Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconContextMenuItem &$menuItem) {
    if (!$options.empty()) {
        if ($options.find("name") != $options.end()) {
            $menuItem.setName($options["name"]);
        }
        if ($options.find("label") != $options.end()) {
            $menuItem.setId($options["label"]);
        }
        if ($options.find("position") != $options.end()) {
            $menuItem.setPosition($options["position"]);
        }
    }
}
