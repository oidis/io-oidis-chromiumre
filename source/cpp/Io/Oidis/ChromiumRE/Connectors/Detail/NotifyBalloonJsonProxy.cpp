/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Io::Oidis::ChromiumRE::Connectors::Detail::NotifyBalloonJsonProxy;

void NotifyBalloonJsonProxy::Update(const json &$options, std::shared_ptr<NotifyBalloonIcon> &$balloonIcon) {
    if (!$options.empty()) {
        if ($options.find("title") != $options.end()) {
            $balloonIcon->setTitle($options["title"]);
        }
        if ($options.find("message") != $options.end()) {
            $balloonIcon->setMessage($options["message"]);
        }
        nlohmann::json::const_iterator item = $options.find("type");
        if (item != $options.end()) {
            if (item.value().is_string()) {
                string tmp = $options.value("type", "");
                if (!tmp.empty()) {
                    std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::toupper);
                    if (tmp == "NONE") {
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::NONE);
                    }
                    if (tmp == "INFO") {
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::INFO);
                    }
                    if (tmp == "WARNING") {
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::WARNING);
                    }
                    if (tmp == "ERROR") {
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::ERROR_TYPE);
                    }
                    if (tmp == "USER") {
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::USER);
                    }
                }
            } else if (item.value().is_number()) {
                switch (item.value().get<int>()) {
                    case 1:
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::INFO);
                        break;
                    case 2:
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::WARNING);
                        break;
                    case 3:
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::ERROR_TYPE);
                        break;
                    case 4:
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::USER);
                        break;
                    default:
                        $balloonIcon->setType(NotifyBalloonIcon::NotifyBalloonIconType::NONE);
                        break;
                }
            }
        }
        if ($options.find("userIcon") != $options.end()) {
            $balloonIcon->setUserIcon($options["userIcon"]);
        }
        if ($options.find("noSound") != $options.end()) {
            $balloonIcon->setNoSound($options["noSound"]);
        }
    }
}
