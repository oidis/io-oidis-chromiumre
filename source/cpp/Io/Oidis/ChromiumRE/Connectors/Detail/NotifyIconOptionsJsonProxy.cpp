/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Io::Oidis::ChromiumRE::Connectors::Detail::NotifyIconOptionsJsonProxy;
using Io::Oidis::ChromiumRE::Connectors::Detail::NotifyBalloonJsonProxy;
using Io::Oidis::ChromiumRE::Connectors::Detail::ContextMenuJsonProxy;
using Io::Oidis::Onion::Gui::NotifyIcon::NotifyBalloonIcon;

void NotifyIconOptionsJsonProxy::Update(const json &$options,
                                        std::shared_ptr<Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconOptions> &$iconOptions) {
    if (!$options.empty()) {
        if ($options.find("tip") != $options.end()) {
            $iconOptions->setTip($options["tip"]);
        }
        if ($options.find("icon") != $options.end()) {
            $iconOptions->setIconPath($options["icon"]);
        }
        if ($options.find("balloon") != $options.end()) {
            auto balloonIcon = std::make_shared<NotifyBalloonIcon>();
            NotifyBalloonJsonProxy::Update($options["balloon"], balloonIcon);
            $iconOptions->setBalloonIcon(balloonIcon);
        }
        if ($options.find("contextMenu") != $options.end()) {
            ContextMenuJsonProxy::Update($options["contextMenu"], $iconOptions);
        }
    }
}
