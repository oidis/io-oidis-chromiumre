/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_CONNECTORS_DETAIL_NOTIFYBALLOONJSONPROXY_HPP_
#define IO_OIDIS_CHROMIUMRE_CONNECTORS_DETAIL_NOTIFYBALLOONJSONPROXY_HPP_

namespace Io::Oidis::ChromiumRE::Connectors::Detail {
    /**
     * WindowHandlerNotifyBalloon class contains properties for NotifyBalloon included in NotifyIcon
     */
    class NotifyBalloonJsonProxy {
        typedef Io::Oidis::Onion::Gui::NotifyIcon::NotifyBalloonIcon NotifyBalloonIcon;

     public:
        /**
         * Update NotifyBalloon properties from JSON options. Only properties explicitly defined in options will be changed.
         * @param $options Specify JSON options.
         */
        static void Update(const json &$options, std::shared_ptr<NotifyBalloonIcon> &$balloonIcon);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_CONNECTORS_DETAIL_NOTIFYBALLOONJSONPROXY_HPP_
