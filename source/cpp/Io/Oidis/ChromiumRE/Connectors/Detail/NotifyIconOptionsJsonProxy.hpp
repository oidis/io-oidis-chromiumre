/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_CONNECTORS_DETAIL_NOTIFYICONOPTIONSJSONPROXY_HPP_
#define IO_OIDIS_CHROMIUMRE_CONNECTORS_DETAIL_NOTIFYICONOPTIONSJSONPROXY_HPP_

namespace Io::Oidis::ChromiumRE::Connectors::Detail {
    /**
     * WindowHandlerNotifyIcon class contains properties of NotifyIcon.
     */
    class NotifyIconOptionsJsonProxy {
     public:
        static void Update(const json &$options, std::shared_ptr<Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconOptions> &$iconOptions);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_CONNECTORS_DETAIL_NOTIFYICONOPTIONSJSONPROXY_HPP_
