/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Io::Oidis::ChromiumRE::Connectors::Detail::ContextMenuJsonProxy;
using Io::Oidis::ChromiumRE::Connectors::Detail::ContextMenuItemJsonProxy;
using Io::Oidis::XCppCommons::Utils::LogIt;
using Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconContextMenuItem;

void ContextMenuJsonProxy::Update(const json &$options,
                                  std::shared_ptr<Io::Oidis::Onion::Gui::NotifyIcon::NotifyIconOptions> &$iconOptions) {
    if (!$options.empty()) {
        if ($options.find("items") != $options.end()) {
            json tmpItems = $options["items"];
            if (!tmpItems.empty() && tmpItems.is_array()) {
                $iconOptions->setContextMenu({});
                for (json::const_iterator it = tmpItems.cbegin(); it != tmpItems.cend(); ++it) {
                    NotifyIconContextMenuItem item;
                    if (it.value().is_object()) {
                        ContextMenuItemJsonProxy::Update(it.value(), item);
                        $iconOptions->AddItem(item);
                    } else {
                        LogIt::Warning("NotifyIcon-ContextMenu item configuration is not object. ({0})", it.value().dump());
                    }
                }
            }
        }
    }
}
