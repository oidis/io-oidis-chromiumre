/* ********************************************************************************************************* *
 *
 * Copyright (c) 2012 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_RENDERER_CLIENTRENDERER_HPP_
#define IO_OIDIS_CHROMIUMRE_RENDERER_CLIENTRENDERER_HPP_

namespace Io::Oidis::ChromiumRE::Renderer {
    class ClientRenderer {
     public:
        static void CreateDelegates(Io::Oidis::ChromiumRE::Renderer::ClientApplicationRenderer::DelegateSet &$delegates);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_RENDERER_CLIENTRENDERER_HPP_
