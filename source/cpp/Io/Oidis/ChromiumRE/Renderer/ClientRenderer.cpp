/* ********************************************************************************************************* *
 *
 * Copyright (c) 2012 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::ChromiumRE::Renderer::ClientApplicationRenderer;

namespace {
    const char kFocusedNodeChangedMessage[] = "ClientRenderer.FocusedNodeChanged";

    class ClientRenderDelegate : public Io::Oidis::ChromiumRE::Interfaces::Delegates::IClientRendererDelegate {
     public:
        ClientRenderDelegate()
                : lastNodeIsEditable(false) {
        }

        void OnWebKitInitialized(CefRefPtr<ClientApplicationRenderer> $app) override {
            CefMessageRouterConfig config;
            this->messageRouter = CefMessageRouterRendererSide::Create(config);
        }

        void OnContextCreated(CefRefPtr<ClientApplicationRenderer> $app, CefRefPtr<CefBrowser> $browser, CefRefPtr<CefFrame> $frame,
                              CefRefPtr<CefV8Context> $context) override {
            this->messageRouter->OnContextCreated($browser, $frame, $context);
        }

        void OnContextReleased(CefRefPtr<ClientApplicationRenderer> $app, CefRefPtr<CefBrowser> $browser,
                               CefRefPtr<CefFrame> $frame, CefRefPtr<CefV8Context> $context) override {
            this->messageRouter->OnContextReleased($browser, $frame, $context);
        }

        void OnFocusedNodeChanged(CefRefPtr<ClientApplicationRenderer> $app, CefRefPtr<CefBrowser> $browser,
                                  CefRefPtr<CefFrame> $frame, CefRefPtr<CefDOMNode> $node) override {
            bool is_editable = ($node.get() && $node->IsEditable());
            if (is_editable != this->lastNodeIsEditable) {
                this->lastNodeIsEditable = is_editable;
                CefRefPtr<CefProcessMessage> message = CefProcessMessage::Create(kFocusedNodeChangedMessage);
                message->GetArgumentList()->SetBool(0, is_editable);
                $frame->SendProcessMessage(PID_BROWSER, message);
            }
        }

        bool OnProcessMessageReceived(CefRefPtr<ClientApplicationRenderer> $app, CefRefPtr<CefBrowser> $browser,
                                      CefRefPtr<CefFrame> $frame, CefProcessId $sourceProcess,
                                      CefRefPtr<CefProcessMessage> $message) override {
            return this->messageRouter->OnProcessMessageReceived($browser, $frame, $sourceProcess, $message);
        }

     private:
        bool lastNodeIsEditable = false;
        CefRefPtr<CefMessageRouterRendererSide> messageRouter;

     IMPLEMENT_REFCOUNTING(ClientRenderDelegate);
    };
}

namespace Io::Oidis::ChromiumRE::Renderer {
    void ClientRenderer::CreateDelegates(ClientApplicationRenderer::DelegateSet &$delegates) {
        $delegates.insert(new ClientRenderDelegate);
    }
}
