/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_ENUMS_TASKBARPROGRESSSTATE_HPP_
#define IO_OIDIS_CHROMIUMRE_ENUMS_TASKBARPROGRESSSTATE_HPP_

#ifdef ERROR
#undef ERROR
#endif

namespace Io::Oidis::ChromiumRE::Enums {
    class TaskBarProgressState
            : public Io::Oidis::XCppCommons::Primitives::BaseEnum<Io::Oidis::ChromiumRE::Enums::TaskBarProgressState> {
     WUI_ENUM_DECLARE(TaskBarProgressState);
     public:
        static const TaskBarProgressState NOPROGRESS;
        static const TaskBarProgressState INDETERMINATE;
        static const TaskBarProgressState NORMAL;
        static const TaskBarProgressState ERROR;
        static const TaskBarProgressState PAUSED;
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_ENUMS_TASKBARPROGRESSSTATE_HPP_
