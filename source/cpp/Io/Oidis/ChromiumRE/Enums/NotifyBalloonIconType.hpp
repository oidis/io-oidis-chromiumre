///* ********************************************************************************************************* *
// *
// * Copyright (c) 2017 NXP
// * Copyright (c) 2019 Oidis
// *
// * SPDX-License-Identifier: BSD-3-Clause
// * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
// * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
// *
// * ********************************************************************************************************* */
//
#ifndef IO_OIDIS_CHROMIUMRE_ENUMS_NOTIFYBALLOONICONTYPE_HPP_
#define IO_OIDIS_CHROMIUMRE_ENUMS_NOTIFYBALLOONICONTYPE_HPP_
//
// #ifdef ERROR
// #undef ERROR
// #endif
//
// namespace Io::Oidis::ChromiumRE::Enums {
//    class NotifyBalloonIconType
//            : public Io::Oidis::XCppCommons::Primitives::BaseEnum<Io::Oidis::ChromiumRE::Enums::NotifyBalloonIconType> {
//     WUI_ENUM_DECLARE(NotifyBalloonIconType)
//
//     public:
//        static const NotifyBalloonIconType NONE;
//        static const NotifyBalloonIconType INFO;
//        static const NotifyBalloonIconType WARNING;
//        static const NotifyBalloonIconType ERROR;
//        static const NotifyBalloonIconType USER;
//    };
// }
//
#endif  // IO_OIDIS_CHROMIUMRE_ENUMS_NOTIFYBALLOONICONTYPE_HPP_
