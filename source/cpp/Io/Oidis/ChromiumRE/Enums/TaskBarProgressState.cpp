/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::XCppCommons::Primitives::BaseEnum;
using Io::Oidis::ChromiumRE::Enums::TaskBarProgressState;

WUI_ENUM_IMPLEMENT(TaskBarProgressState);

WUI_ENUM_CONST_IMPLEMENT(TaskBarProgressState, NOPROGRESS);
WUI_ENUM_CONST_IMPLEMENT(TaskBarProgressState, INDETERMINATE);
WUI_ENUM_CONST_IMPLEMENT(TaskBarProgressState, NORMAL);
WUI_ENUM_CONST_IMPLEMENT(TaskBarProgressState, ERROR);
WUI_ENUM_CONST_IMPLEMENT(TaskBarProgressState, PAUSED);
