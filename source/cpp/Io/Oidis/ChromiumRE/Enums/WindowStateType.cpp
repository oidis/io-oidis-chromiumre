/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::XCppCommons::Primitives::BaseEnum;
using Io::Oidis::ChromiumRE::Enums::WindowStateType;

WUI_ENUM_IMPLEMENT(WindowStateType);

WUI_ENUM_CONST_IMPLEMENT(WindowStateType, MINIMIZED, "minimized");
WUI_ENUM_CONST_IMPLEMENT(WindowStateType, NORMAL, "normal");
WUI_ENUM_CONST_IMPLEMENT(WindowStateType, MAXIMIZED, "maximized");
