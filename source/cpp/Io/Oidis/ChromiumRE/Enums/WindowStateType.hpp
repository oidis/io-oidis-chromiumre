/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_ENUMS_WINDOWSTATETYPE_HPP_
#define IO_OIDIS_CHROMIUMRE_ENUMS_WINDOWSTATETYPE_HPP_

namespace Io::Oidis::ChromiumRE::Enums {
    class WindowStateType
            : public Io::Oidis::XCppCommons::Primitives::BaseEnum<Io::Oidis::ChromiumRE::Enums::WindowStateType> {
     WUI_ENUM_DECLARE(WindowStateType);
     public:
        static const WindowStateType MINIMIZED;
        static const WindowStateType NORMAL;
        static const WindowStateType MAXIMIZED;
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_ENUMS_WINDOWSTATETYPE_HPP_
