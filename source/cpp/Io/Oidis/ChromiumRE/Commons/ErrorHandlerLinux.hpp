/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_COMMONS_ERRORHANDLERLINUX_HPP_
#define IO_OIDIS_CHROMIUMRE_COMMONS_ERRORHANDLERLINUX_HPP_

#ifdef LINUX_PLATFORM

#include <X11/Xlib.h>

#undef Success

namespace Io::Oidis::ChromiumRE::Commons::ErrorHandler {
    int XErrorHandlerImpl(Display *$display, XErrorEvent *$event);

    int XIOErrorHandlerImpl(Display *$display);
}

#endif  // LINUX_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_COMMONS_ERRORHANDLERLINUX_HPP_
