/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Commons {
    CookieVisitor::CookieVisitor(const std::function<void()> &$onCookiesLoaded,
                                 std::vector<CefCookie> *$cookies, bool $usePathFilter)
            : onCookiesLoaded($onCookiesLoaded),
              usePathFilter($usePathFilter),
              cookies($cookies) {}

    CookieVisitor::~CookieVisitor() {
        if (this->onCookiesLoaded) {
            this->onCookiesLoaded();
        }
    }

    bool CookieVisitor::Visit(const CefCookie &$cookie, int $count, int $total, bool &$deleteCookie) {  // NOLINT
        if (!this->usePathFilter || boost::iequals(CefString(&$cookie.path).ToString(),
                                                   Commons::Configuration::getInstance().getAppExeName())) {
            if (this->cookies) {
                this->cookies->emplace_back(CefCookie($cookie));
            }
        }
        return true;  // return true to continue visiting (if other cookie exists);
    }
}
