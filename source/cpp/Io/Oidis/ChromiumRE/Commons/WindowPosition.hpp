/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_COMMONS_WINDOWPOSITION_HPP_
#define IO_OIDIS_CHROMIUMRE_COMMONS_WINDOWPOSITION_HPP_

namespace Io::Oidis::ChromiumRE::Commons {
    using Io::Oidis::ChromiumRE::Browser::RootWindow;

    class WindowPosition {
     public:
        WindowPosition() {}

        void LoadDefault(bool $headless);

        void Load(CefRefPtr<CefCookieManager> $manager,
                  RootWindow *$targetWindow);

        void Save(CefRefPtr<CefCookieManager> $manager);

        void getData(std::vector<CefCookie> *$cc);

        int getX();

        void setX(int $val);

        int getY();

        void setY(int $val);

        int getWidth();

        void setWidth(int $val);

        int getHeight();

        void setHeight(int $val);

        bool getIsMaximized();

        void setIsMaximized(bool $value);

        int getWidthDefault() const {
            return this->widthDefault;
        }

        int getHeightDefault() const {
            return this->heightDefault;
        }

        int getXDefault() const {
#ifdef WIN_PLATFORM
            return CW_USEDEFAULT;
#elif LINUX_PLATFORM
            return 0;
#elif MAC_PLATFORM
            return 0;
#endif
            return 0;
        }

        int getYDefault() const {
#ifdef WIN_PLATFORM
            return CW_USEDEFAULT;
#elif LINUX_PLATFORM
            return 0;
#elif MAC_PLATFORM
            return UtilMac::GetVisibleFrame(nullptr).size.height - this->getHeight() - this->getY();
#endif
            return 0;
        }

     private:
        const CefString url = "http://chromiumre.wuiframework.com";
        const std::vector<string> keys = {"w.x", "w.y", "w.w", "w.h", "w.m"};

        int widthDefault = 1260;
        int heightDefault = 710;

        std::vector<CefCookie> cookies;
        std::map<string, Io::Oidis::ChromiumRE::Commons::CookieElement<int>> map;

        string findValidKey(CefCookie *$cookie);

        int getKeyValue(string $key);

        void setKeyValue(string $key, int $val);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_COMMONS_WINDOWPOSITION_HPP_
