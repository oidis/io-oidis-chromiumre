/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_COMMONS_UTILWIN_HPP_
#define IO_OIDIS_CHROMIUMRE_COMMONS_UTILWIN_HPP_

#ifdef WIN_PLATFORM

namespace Io::Oidis::ChromiumRE::Commons::UtilWin {
    void SetUserDataPtr(HWND $hWnd, void *$ptr);

    template<typename T>
    T GetUserDataPtr(HWND $hWnd) {
        return reinterpret_cast<T>(GetWindowLongPtr($hWnd, GWLP_USERDATA));
    }
}

#endif  // WIN_PLATFORM

#endif  // IO_OIDIS_CHROMIUMRE_COMMONS_UTILWIN_HPP_
