/* ********************************************************************************************************* *
 *
 * Copyright (c) 2015 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Commons {

    void UtilWin::SetUserDataPtr(HWND $hWnd, void *$ptr) {
        SetLastError(ERROR_SUCCESS);
        LONG_PTR result = ::SetWindowLongPtr($hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>($ptr));
        CHECK(result != 0 || GetLastError() == ERROR_SUCCESS);
    }
}

#endif  // WIN_PLATFORM
