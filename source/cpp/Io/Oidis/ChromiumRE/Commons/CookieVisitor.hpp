/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_COMMONS_COOKIEVISITOR_HPP_
#define IO_OIDIS_CHROMIUMRE_COMMONS_COOKIEVISITOR_HPP_

#include "WaitableEvent.hpp"

namespace Io::Oidis::ChromiumRE::Commons {
    class CookieVisitor : public CefCookieVisitor {
     public:
        CookieVisitor(const std::function<void()> &$onCookiesLoaded,
                      std::vector<CefCookie> *$cookies,
                      bool $usePathFilter = true);

        ~CookieVisitor();

        bool Visit(const CefCookie &$cookie, int $count, int $total, bool &$deleteCookie) override;

     private:
        std::function<void()> onCookiesLoaded = nullptr;
        bool usePathFilter;
        std::vector<CefCookie> *cookies = nullptr;

        IMPLEMENT_REFCOUNTING(CookieVisitor);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_COMMONS_COOKIEVISITOR_HPP_
