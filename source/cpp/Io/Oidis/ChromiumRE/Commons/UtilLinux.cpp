/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include <regex>

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Commons {
    using Io::Oidis::XCppCommons::EnvironmentArgs;
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::XCppCommons::Utils::LogIt;

    string UtilLinux::GetAppIconPath() {
        string iconPath;
        const string desktopFilePath = EnvironmentArgs::getInstance().getExecutablePath() + "/" +
                                       EnvironmentArgs::getInstance().getExecutableName() + ".desktop";

        if (FileSystem::Exists(desktopFilePath)) {
            std::ifstream desktopFile(desktopFilePath);
            const std::string content((std::istreambuf_iterator<char>(desktopFile)), std::istreambuf_iterator<char>());

            const std::regex patternToSearch("Icon=(.+)\n");
            std::smatch matches;
            if (std::regex_search(std::cbegin(content), std::cend(content), matches, patternToSearch)) {
                iconPath = matches[1];
            }
        }

        if (iconPath.empty() || !FileSystem::Exists(iconPath)) {
            iconPath = EnvironmentArgs::getInstance().getExecutablePath() + "/../../resource/graphics/icon.ico";

            LogIt::Warning("Icon path does not exist, falling back to the icon from resources: {0}", iconPath);
        }

        return iconPath;
    }
}

#endif  // LINUX_PLATFORM
