/* ********************************************************************************************************* *
 *
 * Copyright (c) 2013 The Chromium Embedded Framework Authors
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_COMMONS_CLIENTAPPLICATION_HPP_
#define IO_OIDIS_CHROMIUMRE_COMMONS_CLIENTAPPLICATION_HPP_

namespace Io::Oidis::ChromiumRE::Commons {
    /**
     * Base class for customizing process-type-based behavior.
     */
    class ClientApplication : public CefApp {
     public:
        ClientApplication() = default;

        /**
         * Determine the process type based on command-line arguments.
         * @param $commandLine
         * @return Returns process type.
         */
        static Io::Oidis::ChromiumRE::Enums::ProcessType GetProcessType(CefRefPtr<CefCommandLine> $commandLine);

     protected:
        std::vector<CefString> cookieableSchemes;

     private:
        static void RegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> $registrar, std::vector<CefString> &$cookiableSchemes);  // NOLINT

        void OnRegisterCustomSchemes(CefRawPtr<CefSchemeRegistrar> $registrar) override;

        DISALLOW_COPY_AND_ASSIGN(ClientApplication);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_COMMONS_CLIENTAPPLICATION_HPP_
