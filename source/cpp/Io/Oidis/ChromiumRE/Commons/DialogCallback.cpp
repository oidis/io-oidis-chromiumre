/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2018-2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Commons {
    using Io::Oidis::ChromiumRE::Connectors::QueryResponse;

    DialogCallback::DialogCallback(shared_ptr<QueryResponse> $response)
        : response($response) {
    }

    void DialogCallback::OnFileDialogDismissed(int /*$selectedAcceptFilter*/, const std::vector<CefString> &$filePaths) {
        CEF_REQUIRE_UI_THREAD();
        if (this->response != nullptr) {
            json arr = json::array();
            std::for_each($filePaths.begin(), $filePaths.end(), [&](const CefString &$item) {
                arr.emplace_back($item.ToString());
            });

            this->response->Send(!$filePaths.empty(), json::array({arr}));
        }
    }
}
