/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_COMMONS_CLIENTAPPLICATIONHYBRID_HPP_
#define IO_OIDIS_CHROMIUMRE_COMMONS_CLIENTAPPLICATIONHYBRID_HPP_

#include "../Browser/ClientApplicationBrowser.hpp"
#include "../Renderer/ClientApplicationRenderer.hpp"

namespace Io::Oidis::ChromiumRE::Commons {
    /**
     * Hybrid architecture that contains both browser and renderer application handlers, intended mainly for single-process mode.
     */
    class ClientApplicationHybrid : public CefApp {
     public:
        ClientApplicationHybrid() = default;

     private:
        CefRefPtr<CefBrowserProcessHandler> GetBrowserProcessHandler() override;

        CefRefPtr<CefRenderProcessHandler> GetRenderProcessHandler() override;

        CefRefPtr<CefBrowserProcessHandler> browserApp = new Io::Oidis::ChromiumRE::Browser::ClientApplicationBrowser();
        CefRefPtr<CefRenderProcessHandler> rendererApp = new Io::Oidis::ChromiumRE::Renderer::ClientApplicationRenderer();

        IMPLEMENT_REFCOUNTING(ClientApplicationHybrid);
        DISALLOW_COPY_AND_ASSIGN(ClientApplicationHybrid);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_COMMONS_CLIENTAPPLICATIONHYBRID_HPP_
