/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2018-2019 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_COMMONS_DIALOGCALLBACK_HPP_
#define IO_OIDIS_CHROMIUMRE_COMMONS_DIALOGCALLBACK_HPP_

namespace Io::Oidis::ChromiumRE::Commons {
    class DialogCallback : public CefRunFileDialogCallback {
     public:
        explicit DialogCallback(shared_ptr<Io::Oidis::ChromiumRE::Connectors::QueryResponse> $response);

        void OnFileDialogDismissed(int $selectedAcceptFilter, const std::vector<CefString> &$filePaths) override;

     private:
         shared_ptr<Io::Oidis::ChromiumRE::Connectors::QueryResponse> response = nullptr;

         IMPLEMENT_REFCOUNTING(DialogCallback);
         DISALLOW_COPY_AND_ASSIGN(DialogCallback);
    };
}

#endif  // IO_OIDIS_CHROMIUMRE_COMMONS_DIALOGCALLBACK_HPP_
