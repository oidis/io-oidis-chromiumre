/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Io::Oidis::ChromiumRE::Commons {
    using Io::Oidis::ChromiumRE::Commons::CookieElement;
    using Io::Oidis::ChromiumRE::Enums::WindowStateType;

    string WindowPosition::findValidKey(CefCookie *$cookie) {
        string retVal;
        for (const auto &key : keys) {
            if (boost::algorithm::iequals(key, CefString(&$cookie->name).ToString())) {
                retVal = key;
                break;
            }
        }
        return retVal;
    }

    void WindowPosition::setKeyValue(string $key, int $val) {
        if (this->map.find($key) == this->map.end()) {
            // key not found -> create new
            CookieElement<int> ce(new CefCookie());
            ce.setPath(Commons::Configuration::getInstance().getAppExeName());
            ce.setName($key);
            this->map.emplace($key, ce);
        }
        this->map[$key].setValue($val);
    }

    int WindowPosition::getKeyValue(string $key) {
        if (this->map.find($key) == this->map.end()) {
            // key not found -> create new
            CookieElement<int> ce(new CefCookie());
            ce.setPath(Commons::Configuration::getInstance().getAppExeName());
            ce.setName($key);
            this->map.emplace($key, ce);
        }
        return this->map[$key].getValue();
    }

    void WindowPosition::Load(CefRefPtr<CefCookieManager> $manager,
                              RootWindow *$targetWindow) {
        this->LoadDefault($targetWindow->isHeadless());

        CefString cUrl = this->url.ToString() + Commons::Configuration::getInstance().getAppExeName();

        const auto onCookiesLoadedCallback = [this, $targetWindow]() {
            if (!$targetWindow->isHeadless()) {
                for (auto &cookie : this->cookies) {
                    string key = this->findValidKey(&cookie);
                    if (!key.empty()) {
                        this->map[key].setCookie(&cookie);
                    }
                }

                const int x = (this->map.find("w.x") == this->map.end() ? this->getXDefault() : this->getX());
                const int y = (this->map.find("w.y") == this->map.end() ? this->getYDefault() : this->getY());
                const int w = (this->map.find("w.w") == this->map.end() ? this->getWidthDefault() : this->getWidth());
                const int h = (this->map.find("w.h") == this->map.end() ? this->getHeightDefault() : this->getHeight());
                const bool m = (this->map.find("w.m") == this->map.end() ? this->getIsMaximized() : false);

                $targetWindow->SetBounds(x, y, w, h);
                $targetWindow->Show(m ? WindowStateType::MAXIMIZED : WindowStateType::NORMAL);
            }
        };

        if (!$manager->VisitUrlCookies(cUrl, false, new Commons::CookieVisitor(onCookiesLoadedCallback, &this->cookies))) {
            onCookiesLoadedCallback();
        }
    }

    void WindowPosition::LoadDefault(bool $headless) {
        this->widthDefault = Commons::Configuration::getInstance().getWindowWidth();
        this->heightDefault = Commons::Configuration::getInstance().getWindowHeight();
        this->setWidth(this->widthDefault);
        this->setHeight(this->heightDefault);
        if ($headless) {
            this->setX(5000);
            this->setY(5000);
        } else {
            this->setX(this->getXDefault());
            this->setY(this->getYDefault());
        }

        this->setIsMaximized(Commons::Configuration::getInstance().isMaximized());
    }

    void WindowPosition::Save(CefRefPtr<CefCookieManager> $manager) {
        std::vector<CefCookie> cc;
        this->getData(&cc);

        CefTime now;
        now.Now();

        for (auto &i : cc) {
            CefString cUrl = this->url.ToString() + Commons::Configuration::getInstance().getAppExeName();
            i.has_expires = 1;
            i.secure = 0;

            i.expires.year = now.year + 1;
            i.expires.day_of_month = now.day_of_month;
            i.expires.month = now.month;

            $manager->SetCookie(cUrl, i, nullptr);
        }

        $manager->FlushStore(nullptr);
    }

    void WindowPosition::getData(std::vector<CefCookie> *$cc) {
        $cc->clear();
        // cppcheck-suppress postfixOperator
        for (std::map<string, CookieElement<int>>::const_iterator it = this->map.begin(); it != this->map.end(); it++) {
            CookieElement<int> item = it->second;
            CefCookie e(*item.getCookie());

            $cc->push_back(e);
        }
    }

    int WindowPosition::getX() {
        return this->getKeyValue("w.x");
    }

    void WindowPosition::setX(int $val) {
        this->setKeyValue("w.x", $val);
    }

    int WindowPosition::getY() {
        return this->getKeyValue("w.y");
    }

    void WindowPosition::setY(int $val) {
        this->setKeyValue("w.y", $val);
    }

    int WindowPosition::getWidth() {
        return this->getKeyValue("w.w");
    }

    void WindowPosition::setWidth(int $val) {
        this->setKeyValue("w.w", $val);
    }

    int WindowPosition::getHeight() {
        return this->getKeyValue("w.h");
    }

    void WindowPosition::setHeight(int $val) {
        this->setKeyValue("w.h", $val);
    }

    bool WindowPosition::getIsMaximized() {
        return static_cast<bool>(this->getKeyValue("w.m"));
    }

    void WindowPosition::setIsMaximized(bool $value) {
        this->setKeyValue("w.m", static_cast<int>($value));
    }
}
