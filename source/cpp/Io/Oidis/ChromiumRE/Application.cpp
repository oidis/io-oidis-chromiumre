/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "sourceFilesMap.hpp"

#include "include/cef_version.h"

#ifdef LINUX_PLATFORM

#include <X11/Xlib.h>

#endif

namespace Io::Oidis::ChromiumRE {
    using Io::Oidis::ChromiumRE::Commons::ClientApplication;
    using Io::Oidis::ChromiumRE::Browser::ClientApplicationBrowser;
    using Io::Oidis::ChromiumRE::Renderer::ClientApplicationRenderer;
    using Io::Oidis::ChromiumRE::Commons::ClientApplicationOther;
    using Io::Oidis::ChromiumRE::Commons::ClientApplicationHybrid;
    using Io::Oidis::ChromiumRE::Commons::Configuration;
    using Io::Oidis::ChromiumRE::Browser::MainContextImpl;
    using Io::Oidis::ChromiumRE::Browser::MainMessageLoop;
    using Io::Oidis::ChromiumRE::Browser::MainMessageLoopStd;
    using Io::Oidis::ChromiumRE::Structures::ChromiumArgs;

    using Io::Oidis::XCppCommons::EnvironmentArgs;
    using Io::Oidis::XCppCommons::Utils::ArgsParser;
    using Io::Oidis::XCppCommons::Utils::LogIt;
    using Io::Oidis::XCppCommons::Enums::LogLevel;
    using Io::Oidis::XCppCommons::Enums::IOHandlerType;
    using Io::Oidis::XCppCommons::IOApi::IOHandlerFactory;
    using Io::Oidis::XCppCommons::Utils::IpcPipeObserver;
    using Io::Oidis::XCppCommons::Primitives::String;
    using Io::Oidis::XCppCommons::System::IO::FileSystem;
    using Io::Oidis::XCppCommons::System::Net::Url;
    using Io::Oidis::ChromiumRE::Enums::ProcessType;

    namespace fs = boost::filesystem;

#ifdef ERROR
#undef ERROR  // un-define ERROR for that scope (is defined in cef_logging.h)
#endif

    int Application::Run(int $argc, const char **$argv) {
        ChromiumArgs args;
        CefEnableHighDPISupport();
        CefRefPtr<CefCommandLine> commandLine = CefCommandLine::CreateCommandLine();

#ifdef WIN_PLATFORM
        CefMainArgs mainArgs(GetModuleHandle(nullptr));

        commandLine->InitFromString(::GetCommandLineW());

#elif defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
        CefMainArgs mainArgs($argc, const_cast<char **>($argv));

        commandLine->InitFromArgv($argc, $argv);
#endif

#ifdef MAC_PLATFORM
#warning "Application::Run() - Setting single-process mode on OS X (multi process mode is not implemented)"
        Configuration::getInstance().setSingleProcess(true);

        NSAutoreleasePool* autopool = [[NSAutoreleasePool alloc] init];

        // explicitly instantiate custom NSApplication
        [ApplicationHandlerMac sharedApplication];

        ApplicationDelegateMac *applicationDelegate = [[ApplicationDelegateMac alloc] init];
#endif

        CefRefPtr<CefApp> app = nullptr;
        const ProcessType processType = ClientApplication::GetProcessType(commandLine);

        if (Configuration::getInstance().IsSingleProcess()) {
            LogIt::Info("Starting hybrid process (browser and renderer).");

            app = new ClientApplicationHybrid();
        } else {
            if (processType == ProcessType::BrowserProcess) {
                LogIt::Info("Starting browser process.");

                app = new ClientApplicationBrowser();
            } else if (processType == ProcessType::RendererProcess || processType == ProcessType::ZygoteProcess) {
                LogIt::Info("Starting renderer process.");

                app = new ClientApplicationRenderer();

            } else if (processType == ProcessType::OtherProcess) {
                LogIt::Info("Starting other process.");

                app = new ClientApplicationOther();
            }
        }

        const int exitCode = CefExecuteProcess(mainArgs, app, nullptr);
        if (exitCode >= 0) {
            LogIt::Debug("Exit process type: {0} with exit code {1}.", processType, exitCode);
            return exitCode;
        }

        const int parseResult = ArgsParser::Parse(args, $argc, $argv);
        if (parseResult == 1) {
            return 0;
        }

        if (args.getConnectorPID() > 0) {
            LogIt::Info("Registered connector PID: " + std::to_string(args.getConnectorPID()));
        }

        scoped_ptr<MainContextImpl> context(new MainContextImpl(nullptr, true));

        CefSettings settings;
        settings.no_sandbox = 1;
        settings.command_line_args_disabled = 1;
        settings.multi_threaded_message_loop = 0;
#ifdef NDEBUG
        settings.log_severity = cef_log_severity_t::LOGSEVERITY_INFO;
#else
        settings.log_severity = cef_log_severity_t::LOGSEVERITY_VERBOSE;
#endif
        if (args.getRemoteDebuggingPort() > 0) {
            settings.remote_debugging_port = args.getRemoteDebuggingPort();
            Configuration::getInstance().setRemoteDebuggingPort(args.getRemoteDebuggingPort());
        }

        context->PopulateSettings(&settings);

        const fs::path cachePath = fs::path(FileSystem::getLocalAppDataPath()) /
                                   string(EnvironmentArgs::getInstance().getProjectName());

        try {
            if (!FileSystem::Exists(cachePath.string())) {
                FileSystem::CreateDirectory(cachePath.string());
            }

            if (fs::is_directory(cachePath)) {
                CefString(&settings.cache_path) = string(cachePath.string());
                CefString(&settings.user_data_path) = string(cachePath.string());
                settings.persist_session_cookies = 1;
                settings.persist_user_preferences = 1;
                LogIt::Info("Setting cookies path \"{0}\"", cachePath.string());
            } else {
                LogIt::Warning("Can not create cache path at \"" + cachePath.string() + "\"");
            }
        }
        catch (fs::filesystem_error const &e) {
            LogIt::Error(e.what());
        }

        std::ostringstream agentStream;
        agentStream << "Chrome/" << CHROME_VERSION_MAJOR << "." << CHROME_VERSION_MINOR
                    << " com-wui-framework-jre/1.0.0";
        CefString(&settings.product_version) = agentStream.str();
        LogIt::Debug("Product version/agentstream: {0}", agentStream.str().c_str());

        string extUrl = args.getTarget();
        string extQuery, extHash;
        try {
            Url url(extUrl);
            if (!url.getQuery().empty() || !url.getHash().empty()) {
                extUrl = url.getPathname();
                extQuery = url.getQuery();
                extHash = url.getHash();
            }

            fs::path path(extUrl);

            if (path.is_relative()) {
                path = fs::system_complete(path);
            }

            if (fs::is_directory(path)) {
                path = path.append("index.html");
            }

            if (!fs::exists(path)) {
                extUrl = "unknown";
                LogIt::Warning("Can not locate target path for target argument: \"{0}\"", args.getTarget());
            } else {
                extUrl = string(path.string());
                LogIt::Info("Found target path \"{0}\"", extUrl);
            }
        } catch (fs::filesystem_error const &e) {
            extUrl = "unknown" + string(e.what());
            LogIt::Error(e.what());
        }

        if (!args.getQuery().empty()) {
            extQuery = Url::Decode(args.getQuery());
        }
        if (!args.getHash().empty()) {
            extHash = Url::Decode(args.getHash());
        }

        LogIt::Info("\n\tQuery: {0}\n\tHash: {1}", extQuery, extHash);

        fs::path appExeName = EnvironmentArgs::getInstance().getExecutableName();
        string appName = appExeName.string();
        string releaseName;
#ifdef WIN_PLATFORM
        string platform = "win32";
#elif LINUX_PLATFORM
        string platform = "linux";
#elif MAC_PLATFORM
        string platform = "mac";
#endif

        fs::path configPath(extUrl);
        configPath = configPath.parent_path();
        configPath = configPath.append("wuirunner.config.jsonp");
        if (FileSystem::Exists(configPath.string())) {
            const string content = FileSystem::Read(configPath.string());
            LogIt::Info("Loaded wuirunner.config.jsonp\n{0}", content);

            json data = Io::Oidis::XCppCommons::Utils::JSON::ParseJsonp(content);
            if (data.find("startPage") != data.end()) {
                Commons::Configuration::getInstance().setStartPage(data["startPage"]);
            }
            if (data.find("window") != data.end()) {
                json window = data["window"];
                if (window.find("title") != window.end()) {
                    appName = window["title"];
                }
                if (window.find("width") != window.end()) {
                    Commons::Configuration::getInstance().setWindowWidth(boost::lexical_cast<int>(window["width"].get<string>()));
                }
                if (window.find("height") != window.end()) {
                    Commons::Configuration::getInstance().setWindowHeight(boost::lexical_cast<int>(window["height"].get<string>()));
                }
                if (window.find("maximized") != window.end()) {
                    bool isMaximized = false;
                    const string maxStr = window["maximized"].get<string>();
                    if (String::ContainsIgnoreCase(maxStr, std::vector<string>{"true", "1"})) {
                        isMaximized = true;
                    }
                    Commons::Configuration::getInstance().setMaximized(isMaximized);
                }
            }
            if (data.find("releaseName") != data.end()) {
                releaseName = data["releaseName"];
            }
            if (data.find("platform") != data.end()) {
                platform = data["platform"];
            }
        }

        extUrl = String::Replace(extUrl, "\\", "/");
        Commons::Configuration::getInstance().setTargetFilePath(extUrl);
#ifdef WIN_PLATFORM
        const auto currentPID = std::to_string(GetCurrentProcessId());
#elif defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
        const auto currentPID = std::to_string(getpid());
#endif

        string query = "AppName=" + appName + "&" +
                       "AppPid=" + currentPID + "&" +
                       "ReleaseName=" + releaseName + "&" +
                       "Platform=" + platform;
        if (!extQuery.empty()) {
            query += "&" + extQuery;
        }
        if (!extHash.empty()) {
            if (String::StartsWith(extHash, "#")) {
                extHash = String::Substring(extHash, 1);
            }
            Commons::Configuration::getInstance().setStartPage(extHash);
        }
        Commons::Configuration::getInstance().setApplicationQuery(query);
        Commons::Configuration::getInstance().setAppExeName("/" + String::Replace(appExeName.string(), " ", ""));
        LogIt::Debug("Application executable name used for cookies/path {0}.)", Commons::Configuration::getInstance().getAppExeName());

        scoped_ptr<MainMessageLoop> messageLoop(new MainMessageLoopStd);

        context->Initialize(CefMainArgs(), settings, app, nullptr);

#ifdef LINUX_PLATFORM
        gtk_init(&$argc, const_cast<char ***>(&$argv));

        namespace ErrorHandler = Io::Oidis::ChromiumRE::Commons::ErrorHandler;

        XSetErrorHandler(ErrorHandler::XErrorHandlerImpl);
        XSetIOErrorHandler(ErrorHandler::XIOErrorHandlerImpl);
#endif

        const auto resolvedTargetUrl = "file://" + Url::Encode(Commons::Configuration::getInstance().getTargetUrl());
        context->getRootWindowManager()->CreateRootWindow(args, resolvedTargetUrl);
        const int result = messageLoop->Run();

        try {
            if (args.getConnectorPID() > 0) {
                LogIt::Debug("Closing WuiConnector with pid: {0}", args.getConnectorPID());
                IpcPipeObserver ipcPipeObserver("WuiConnector", args.getConnectorPID());

                if (ipcPipeObserver.Initialize(false, 0)) {
                    string msg = "--stop #" + currentPID;
                    ipcPipeObserver.Send(msg);
                    LogIt::Debug("Stop request has been send to connector (pid: {0}).", args.getConnectorPID());
                } else {
                    LogIt::Error("Ipc pipe to WuiConnector (pid: {0}) can not be opened.", args.getConnectorPID());
                }
            }
        } catch (std::exception &ex) {
            LogIt::Error(ex);
        }

        context->Shutdown();
        messageLoop.reset();
        context.reset();

#ifdef MAC_PLATFORM
        [applicationDelegate release];
        [autopool release];
#endif

        return result;
    }
}
