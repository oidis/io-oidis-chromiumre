/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_CHROMIUMRE_HPP_  // NOLINT
#define IO_OIDIS_CHROMIUMRE_HPP_

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"
#include "../../dependencies/io-oidis-onion/source/cpp/reference.hpp"

#ifdef WIN_PLATFORM
#include <commdlg.h>  // NOLINT
#include <Shobjidl.h>
#endif  // WIN_PLATFORM

#include "interfacesMap.hpp"
#include "Io/Oidis/ChromiumRE/sourceFilesMap.hpp"

// global-cef-includes-start
#include "include/cef_app.h"
#include "include/cef_base.h"
#include "include/cef_stream.h"
#include "include/cef_browser.h"
#include "include/cef_client.h"
#include "include/cef_task.h"
#include "include/cef_render_handler.h"
#include "include/cef_image.h"
#include "include/cef_request.h"
#include "include/cef_response.h"
#include "include/cef_response_filter.h"
#include "include/cef_resource_handler.h"
#include "include/cef_resource_request_handler.h"
#include "include/cef_request_handler.h"
#include "include/cef_cookie.h"
#include "include/cef_scheme.h"
#include "include/cef_parser.h"
#include "include/cef_browser_process_handler.h"
#include "include/cef_command_line.h"
#include "include/cef_request_context_handler.h"
#include "include/base/cef_scoped_ptr.h"
#include "include/base/cef_lock.h"
#include "include/base/cef_ref_counted.h"
#include "include/base/cef_thread_checker.h"
#include "include/base/cef_bind.h"
#include "include/base/cef_build.h"
#include "include/wrapper/cef_helpers.h"
#include "include/wrapper/cef_message_router.h"
#include "include/wrapper/cef_resource_manager.h"
#include "include/wrapper/cef_closure_task.h"
#include "include/internal/cef_types_wrappers.h"
// global-cef-includes-stop

#include "Io/Oidis/ChromiumRE/Enums/WindowStateType.hpp"
#include "Io/Oidis/ChromiumRE/Commons/CookieElement.hpp"
#include "Io/Oidis/ChromiumRE/Commons/ClientApplication.hpp"
#include "Io/Oidis/ChromiumRE/Commons/WindowPosition.hpp"
#include "Io/Oidis/ChromiumRE/Enums/NotifyBalloonIconType.hpp"
#include "Io/Oidis/ChromiumRE/Enums/TaskBarProgressState.hpp"

// generated-code-start
#include "Io/Oidis/ChromiumRE/Application.hpp"
#include "Io/Oidis/ChromiumRE/ApplicationDelegateMac.hpp"
#include "Io/Oidis/ChromiumRE/ApplicationHandlerMac.hpp"
#include "Io/Oidis/ChromiumRE/Browser/BorderlessWindowMac.hpp"
#include "Io/Oidis/ChromiumRE/Browser/BrowserWindow.hpp"
#include "Io/Oidis/ChromiumRE/Browser/BrowserWindowStdLinux.hpp"
#include "Io/Oidis/ChromiumRE/Browser/BrowserWindowStdMac.hpp"
#include "Io/Oidis/ChromiumRE/Browser/BrowserWindowStdWin.hpp"
#include "Io/Oidis/ChromiumRE/Browser/ClientApplicationBrowser.hpp"
#include "Io/Oidis/ChromiumRE/Browser/ClientHandler.hpp"
#include "Io/Oidis/ChromiumRE/Browser/ClientHandlerStd.hpp"
#include "Io/Oidis/ChromiumRE/Browser/ClientHandlerStdLinux.hpp"
#include "Io/Oidis/ChromiumRE/Browser/ClientTypes.hpp"
#include "Io/Oidis/ChromiumRE/Browser/DebugWindowWin.hpp"
#include "Io/Oidis/ChromiumRE/Browser/DialogHandlerLinux.hpp"
#include "Io/Oidis/ChromiumRE/Browser/MainContext.hpp"
#include "Io/Oidis/ChromiumRE/Browser/MainContextImpl.hpp"
#include "Io/Oidis/ChromiumRE/Browser/MainMessageLoop.hpp"
#include "Io/Oidis/ChromiumRE/Browser/MainMessageLoopStd.hpp"
#include "Io/Oidis/ChromiumRE/Browser/NotifyIconMac.hpp"
#include "Io/Oidis/ChromiumRE/Browser/RootWindow.hpp"
#include "Io/Oidis/ChromiumRE/Browser/RootWindowLinux.hpp"
#include "Io/Oidis/ChromiumRE/Browser/RootWindowMac.hpp"
#include "Io/Oidis/ChromiumRE/Browser/RootWindowManager.hpp"
#include "Io/Oidis/ChromiumRE/Browser/RootWindowWin.hpp"
#include "Io/Oidis/ChromiumRE/Browser/TaskBarMac.hpp"
#include "Io/Oidis/ChromiumRE/Browser/WindowTestRunner.hpp"
#include "Io/Oidis/ChromiumRE/Browser/WindowTestRunnerLinux.hpp"
#include "Io/Oidis/ChromiumRE/Browser/WindowTestRunnerMac.hpp"
#include "Io/Oidis/ChromiumRE/Browser/WindowTestRunnerWin.hpp"
#include "Io/Oidis/ChromiumRE/Commons/ClientApplicationHybrid.hpp"
#include "Io/Oidis/ChromiumRE/Commons/ClientApplicationOther.hpp"
#include "Io/Oidis/ChromiumRE/Commons/Configuration.hpp"
#include "Io/Oidis/ChromiumRE/Commons/CookieVisitor.hpp"
#include "Io/Oidis/ChromiumRE/Commons/DialogCallback.hpp"
#include "Io/Oidis/ChromiumRE/Commons/ErrorHandlerLinux.hpp"
#include "Io/Oidis/ChromiumRE/Commons/SchemeTestCommon.hpp"
#include "Io/Oidis/ChromiumRE/Commons/SourceVisitor.hpp"
#include "Io/Oidis/ChromiumRE/Commons/UtilLinux.hpp"
#include "Io/Oidis/ChromiumRE/Commons/UtilMac.hpp"
#include "Io/Oidis/ChromiumRE/Commons/UtilWin.hpp"
#include "Io/Oidis/ChromiumRE/Commons/WaitableEvent.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/Detail/ContextMenuItemJsonProxy.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/Detail/ContextMenuJsonProxy.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/Detail/NotifyBalloonJsonProxy.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/Detail/NotifyIconOptionsJsonProxy.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/Detail/WindowHandlerFileDialog.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/QueryContentHandler.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/QueryResponse.hpp"
#include "Io/Oidis/ChromiumRE/Connectors/WindowHandler.hpp"
#include "Io/Oidis/ChromiumRE/Enums/ProcessType.hpp"
#include "Io/Oidis/ChromiumRE/Enums/WindowCornerType.hpp"
#include "Io/Oidis/ChromiumRE/Interfaces/Delegates/IBrowserWindowDelegate.hpp"
#include "Io/Oidis/ChromiumRE/Interfaces/Delegates/IClientApplicationDelegate.hpp"
#include "Io/Oidis/ChromiumRE/Interfaces/Delegates/IClientDelegate.hpp"
#include "Io/Oidis/ChromiumRE/Interfaces/Delegates/IClientRendererDelegate.hpp"
#include "Io/Oidis/ChromiumRE/Interfaces/Delegates/IWindowDelegate.hpp"
#include "Io/Oidis/ChromiumRE/Loader.hpp"
#include "Io/Oidis/ChromiumRE/Renderer/ClientApplicationRenderer.hpp"
#include "Io/Oidis/ChromiumRE/Renderer/ClientRenderer.hpp"
#include "Io/Oidis/ChromiumRE/Structures/ChromiumArgs.hpp"
// generated-code-end

#endif  // IO_OIDIS_CHROMIUMRE_HPP_  NOLINT
