/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

const LogIt = Io.Oidis.Commons.Utils.LogIt;
const EnvironmentHelper = Io.Oidis.Localhost.Utils.EnvironmentHelper;
const Loader = Io.Oidis.Builder.Loader;
const terminal = Loader.getInstance().getTerminal();

var cwd;

function installGtk($callback) {
    terminal.Elevate("apt install libgtk2.0-0", [], cwd, function ($exitCode) {
        if ($exitCode !== 0) {
            LogIt.Warning("Gtk failed to install.");
        }
        $callback();
    });
}

function checkForGtk($callback) {
    terminal.Spawn("dpkg -l libgtk2.0-0 > /dev/null", [], cwd, function ($exitCode) {
        if ($exitCode !== 0) {
            LogIt.Warning("Gtk not installed.");
            installGtk($callback);
        } else {
            $callback();
        }
    });
}

function installAppindicator($callback) {
    terminal.Elevate("apt install libappindicator-dev", [], cwd, function ($exitCode) {
        if ($exitCode !== 0) {
            LogIt.Warning("Appindicator failed to install.");
        }
        $callback();
    });
}

function checkForAppindicator($callback) {
    terminal.Spawn("dpkg -l libappindicator-dev > /dev/null", [], cwd, function ($exitCode) {
        if ($exitCode !== 0) {
            LogIt.Warning("Appindicator not installed.");
            installAppindicator($callback);
        } else {
            $callback();
        }
    });
}

function checkChromiumDependencies($callback) {
    var checks = [checkForGtk, checkForAppindicator];

    var run = function ($index) {
        if ($index >= checks.length) {
            $callback();
        } else {
            checks[$index](() => {
                run($index + 1);
            });
        }
    };
    run(0);
}

function copyResourcesNextToLib($callback) {
    terminal.Spawn("cp", ["-r", "Resources/* Release/"], cwd, function ($exitCode) {
        $callback();
    });
}

Process = function ($cwd, $args, $done) {
    if (!EnvironmentHelper.IsLinux()) {
        $done();
    } else {
        cwd = $cwd;

        terminal.Spawn("strip", ["-s", "libcef.so", "libEGL.so", "libGLESv2.so"], $cwd + "/Release", function ($exitCode) {
            if ($exitCode !== 0) {
                LogIt.Warning("Strip failed.");
            }
            checkChromiumDependencies(() => {
                copyResourcesNextToLib($done);
            });
        });
    }
};
