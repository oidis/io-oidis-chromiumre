# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2018 NXP
# * Copyright (c) 2019 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

cmake_minimum_required(VERSION 3.6)

set(CMAKE_CXX_STANDARD 17)

include(resource/scripts/chromiumre_configure.cmake)
include(resource/scripts/chromiumre_utils.cmake)

macro(APPLICATION_MAIN_CALLBACK target)
    target_include_directories(${target} PRIVATE ${_includeDirectories})
    target_compile_definitions(${target} PRIVATE ${_compileDefinitions})
    target_compile_options(${target} PRIVATE ${_compileOptions})
    target_link_libraries(${target} ${_libraries})

    if (WIN32)
        set_target_properties(${target} PROPERTIES WIN32_EXECUTABLE ON)
    endif ()

    if (APPLE)
        _osx_add_target_properties(${target})
        _osx_add_post_build_steps(${target})
    endif ()
endmacro()

macro(APPLICATION_TEST_CALLBACK target testTarget)
    target_include_directories(${target} PRIVATE ${_includeDirectories})
    target_include_directories(${testTarget} PRIVATE ${_includeDirectories})
    target_compile_definitions(${target} PRIVATE ${_compileDefinitions})
    target_compile_definitions(${testTarget} PRIVATE ${_compileDefinitions})
    target_compile_options(${target} PRIVATE ${_compileOptions})
    target_compile_options(${testTarget} PRIVATE ${_compileOptions})
    target_link_libraries(${target} ${_libraries})

    if (APPLE)
        _osx_add_target_properties(${target})
        _osx_add_post_build_steps(${target})
    endif ()
endmacro()

macro(APPLICATION_WUI_DEPENDENCIES_CALLBACK wuiDep)
    target_compile_definitions(libcef_dll_wrapper PRIVATE ${_compileDefinitions})

    target_include_directories(${wuiDep} PRIVATE ${_includeDirectories})
    target_compile_definitions(${wuiDep} PRIVATE ${_compileDefinitions})
    target_compile_options(${wuiDep} PRIVATE ${_compileOptions})
    target_link_libraries(${wuiDep} ${_libraries})
endmacro()

add_subdirectory(bin/resource/scripts)
